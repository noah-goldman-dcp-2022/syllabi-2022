import { openDB } from 'idb'

// Open the saved assignment data as an IDB database


// Store form values in an IndexedDB for later retrieval by `name`
async function saveFormToBrowser(formID = 'form_assignment', storeName = 'assignment') {

  let assignmentDB = await openDB('assignment', {
    upgrade() {
      console.log(`Attempting to create object store 'assignment'…`)
      const store = assignmentDB.createObjectStore('assignment')
      console.log(assignmentDB.objectStoreNames)
    }
  })
  assignmentDB.createObjectStore('assignment')

  // Get the form element
  const formElement = document.getElementById(formID)

  // Create an array of the form's fields
  const formFieldsArray = Array.from(formElement.elements)

  // Store all form field values in DB
  //const tx = assignmentDB.transaction(storeName, 'readwrite')
   //console.log(tx)

  // const store = tx.objectStore(storeName)
  formFieldsArray.map(field => assignmentDB.put('assignment', field.value, field.id))
  // await tx.done
}
/*
async function fillFormFromBrowser(formID = 'form_assignment', dbName = 'assignment', storeName = 'assignment') {

}
*/

export { saveFormToBrowser }