### A Pluto.jl notebook ###
# v0.18.0

using Markdown
using InteractiveUtils

# ╔═╡ 6fdddda2-3ac3-11ec-06b1-8b6384fa662e
md"""
# Julia Introduction
by Noah Scott Goldman

Hey, welcome! This notebook will help you get started programming with Julia.
"""

# ╔═╡ 41d420f1-f4ed-4999-b1e9-543d7b7f0226
HTML("<img src='https://julialang.org/assets/infra/logo.svg' alt='Julia logo' role='img'/>")

# ╔═╡ 7783e58b-c5fc-4e5d-8d9c-44a75f34b72f
md"""
## Why Julia?

Julia is a programming language built to solve hard problems quickly and efficiently. Although it is more complex than your calculator, Julia’s complexity allows you to do much more, much faster.

(Note: Because we are running Julia over the Internet, it will be much slower than if you were running it on your own machine.)

Let’s first try some basic math.
"""

# ╔═╡ 4bcd74b3-1937-4d16-aded-d70bdfb9c836
product = nothing

# ╔═╡ 6e29e667-e4a8-465b-bc22-f3d0722569bc
if product == nothing
	md"*In the cell below, replace* `nothing` *with* `2.0 * 3.25`*. Then press* `Shift`+`Enter` *to run the cell.*"
elseif product == 6.5
	md"""
	*In the cell below, replace* `nothing` *with* `2.0 * 3.25`*. Then press* `Shift`+`Enter` *to run the cell.*
	
	🥳 **Woo-hoo!** You may proceed.
	"""
else
	md"""
	*In the cell below, replace* `nothing` *with* `2.0 * 3.25`*. Then press* `Shift`+`Enter` *to run the cell.*
	
	🪳 **Hmm, not there yet.** Check your code for bugs and see if you can fix it.
	"""
end

# ╔═╡ cb4a0bcf-9c63-4b3e-b4fb-00a7f3236b10
md"""
You can also do more complex calculations.
"""

# ╔═╡ 81294e2d-1bb6-4fb8-97f0-ed72bed0da10
md"""*In the cell below, replace* `nothing` *with the following code. Then press* `Shift`+`Enter` *to run the cell.*
```julia
map(x -> x^2, 1:20)
```
"""

# ╔═╡ 8d036cac-3811-4a51-a1c0-1a8617e4d3ad
squares = nothing

# ╔═╡ 6fe5c559-d6dd-497f-b348-b30ed6734c24
if squares == nothing
	md""
elseif squares == [1, 4, 9, 16, 25, 36, 49, 64, 81, 100, 121, 144, 169, 196, 225, 256, 289, 324, 361, 400]
	md"🥳 **Woo-hoo!** Looks like the squares from $1^2$ to $20^2$ are $(repr(squares))."
else
	md"**Hmm, not there yet.** Check your code for bugs and see if you can fix it."
end

# ╔═╡ 165dcae3-39f4-490b-b75f-087024f22db4
md"""
Julia can do more than just math, though. It can also work with text.
"""

# ╔═╡ f5225f0d-bc5a-4c0b-8291-475405850ad6
my_name = nothing

# ╔═╡ 24ac1846-6253-4f65-af6a-87ea1966c04d
if my_name == nothing
	md"""
	*In the cell below, replace* `nothing` *with your name in double quotes – for example,* `\"Noah\"`. *Then press* `Shift`+`Enter` *to run the cell.*
	"""
elseif typeof(my_name) == String
	md"""
	*In the cell below, replace* `nothing` *with your name in double quotes – for example,* `\"Noah\"`. *Then press* `Shift`+`Enter` *to run the cell.*
	
	🥳 **Hi there,** $(my_name)!
	"""
else
	md"""
	*In the cell below, replace* `nothing` *with your name in double quotes – for example,* `\"Noah\"`. *Then press* `Shift`+`Enter` *to run the cell.*
	
	🪳 **Hmm, not there yet.** Make sure you enclosed your name in double quotes and didn’t use any weird characters.
	"""
end

# ╔═╡ a65263b0-ce04-4343-9c95-b78b2a78094a
md"""
Here’s another example where we *concatenate* text. (Remember that from unit 1?)
"""

# ╔═╡ 5eca37d4-61da-4185-a5bb-c7beb17d9bd4
greeting = nothing

# ╔═╡ a1dedb51-27f9-404b-84c6-ecf8ab7fc3bb
if greeting == nothing
	md"""
	*In the cell below, replace* `nothing` *with the following code. Then press* `Shift`+`Enter` *to run the cell.*
	```julia
	"Happy " * "Monday!"
	```
	"""
elseif greeting == "Happy Monday!"
	md"""
	*In the cell below, replace* `nothing` *with the following code. Then press* `Shift`+`Enter` *to run the cell.*
	```julia
	"Happy " * "Monday!"
	```
	
	🥳 **Happy Monday** to you too, $(my_name)!
	"""
else
	md"""
	*In the cell below, replace* `nothing` *with the following code. Then press* `Shift`+`Enter` *to run the cell.*
	```julia
	"Happy " * "Monday!"
	
	🪳 **Hmm, not there yet.** Check your code for bugs and see if you can fix it.
	"""
end

# ╔═╡ 5b7f7c58-f760-4dc0-bd39-ce1e78129187
md"""
## Types

A **value** is a basic thing that a program works with. Some values we have seen so far are `6.5`, `400`, and `"Happy Monday!"`.

In Julia, every value has a **type**. For example:

  * `400` is an `Int` (short for "integer").
  * `6.5` is a `Float` (short for "floating-point number", because the decimal point is allowed to move around).
  * `"Happy Monday!"` is a `String` (because the characters it contains are strung together).

If you are ever unsure about a value’s type, use the `typeof` function. Let’s practise:
"""

# ╔═╡ eae5d18f-a0b7-44c5-8d95-3b13074779c0
md"""
*In the cell below, replace* `nothing` *with the following code. Then press* `Shift`+`Enter` *to run the cell.*
```julia
typeof(6.5)
```
"""

# ╔═╡ e64095c2-47d0-4cfe-8ecb-a550ee0b44f9
t1 = nothing

# ╔═╡ fd3719d0-e20c-41bf-a3cb-078a7d74e585
if t1 == nothing
	md""
elseif t1 == typeof(6.5)
	md"🥳 **Woo-hoo!** Looks like 6.5 is a $(t1)!"
else
	md"🪳 **Hmm, not there yet.** Check your code for bugs and see if you can fix it."
end

# ╔═╡ ad18ebf1-b2d7-480f-8567-9403825b16ea
md"""
*Now, set* `t2` *equal to the type of* `400`, *and* `t3` *the type of* `"Happy Monday!"`.
"""

# ╔═╡ a6bca36c-6eb2-4288-b9da-04b196b7e620
t2 = nothing

# ╔═╡ b3181dd4-9143-44aa-8eb1-46226c8b4760
if t2 == nothing
	md""
elseif t2 == typeof(400)
	md"🥳 **Woo-hoo!** Looks like 400 is an $(t2)!"
else
	md"🪳 **Hmm, not there yet.** Check your code for bugs and see if you can fix it."
end

# ╔═╡ 8fb10aaa-339d-4fb2-b071-2a4c4e595251
t3 = nothing

# ╔═╡ e349215c-3215-4539-945e-e321ab632bb1
if t3 == nothing
	md""
elseif t3 == String
	md"🥳 **Woo-hoo!** Looks like \"Happy Monday!\" is a $(t3)!"
else
	md"🪳 **Hmm, not there yet.** Check your code for bugs and see if you can fix it."
end

# ╔═╡ d9aa471c-1ca3-4695-badb-ab744d556e26
md"""
Note that the numbers in Julia do not always behave the same as the numbers in math. This is also true of calculators.

For example, in math, 1.0 and 1 are the same number. They are both equal to the integer 1.

However, in Julia, `1.0` is a `Float`, while `1` is an `Int`. Thus, `1.0` and `1` are **not** interchangeable. You may get an error message if you try to use a `Float` where a function expects an `Int`.
"""

# ╔═╡ d1a459ae-e0f4-4958-bc47-249227ba47e5
md"""
## Math in Julia

Here are some basic math operations in Julia. Try running all of them in the cell below.

| Operation      | Julia  example  | Output |
|:---------------|:--------:|:--------------:|
| Addition       | `3 + 2`  | `5`    |
| Subtraction    | `3 - 2`  | `1`   |
| Multiplication | `3 * 2`  | `6`   |
| Division       | `3 / 2`  | `1.5`  |
| Exponentiation | `3 ^ 2`  | `9`  |

Notice that division spits out a `Float` even though both arguments are `Int`. This will be true even when the numerator is an integer multiple of the denominator, such as `9/3`.
"""

# ╔═╡ b2618715-6bc6-48ba-a6a2-af03b07dc41a
3 + 2

# ╔═╡ 9796d4d2-ad68-4eee-8040-7cb512e4823d
md"""
You can also do trigonometry in Julia. Let’s try finding $\tan(45°)$.

*Type* `tan(45)` *into the cell below, then run it.*
"""

# ╔═╡ c11264e1-240e-4dd2-9e99-04ee3d1b6161


# ╔═╡ 1c9f6dec-6934-449f-92d9-a9ed485534cb
md"""
Wait, shouldn’t $\tan(45°) = 1$? Yes, it should. However, in Julia, `cos`, `sin`, and `tan` are in *radian* mode, so we just found the tangent of 45 *radians*. Oops!

To work in *degrees*, we need to use the functions `cosd`, `sind`, and `tand` – the `d` is for "degrees".

*Find the tangent of 45° using* `tand`.
"""

# ╔═╡ a0cfd290-083c-4f1b-be31-5761df0cd547


# ╔═╡ a025bf43-80de-43d4-bca9-b707aebac0f0
md"""
The last function we’ll cover today is called the **factorial**. This is a math concept: if $n$ is a whole number, then $n!$ (pronounced “$n$ factorial”) is defined by

$$n! = 1 \times 2 \times \dotsb \times n$$

For example, $4! = 1 \times 2 \times 3 \times 4 = 24$.

Factorials can quickly get big ( $7! = 5,040$ ) so it’s helpful to have Julia compute them for us. To find the factorial of `n` in Julia, use `factorial(n)`.

*Find $12!$ using Julia.*
"""

# ╔═╡ c863dc6a-2db2-4cc4-a000-43295ac048b0


# ╔═╡ 0aae1a40-fc00-4bbb-afc4-8820aa3728d3
md"""
## Defining functions

You can define functions in Julia.
"""

# ╔═╡ 946186ce-02ed-46cb-b229-23764d2e514e
md"""
*In the cell below, replace `nothing` with* `8*x - 2`, *then run the code.*
"""

# ╔═╡ 2451013b-f7cd-43e9-9c7c-1c6ce60a304c
f(x) = nothing

# ╔═╡ 2311aa43-dffc-4d66-92f8-dd4b9ca7d001
begin
	l = map(f, [202, 719, 3])
	if l == [1614, 5750, 22]
		md"🥳 **Woo-hoo!** You’ve defined your first Julia function!"
	elseif l == [nothing, nothing, nothing]
		md""
	else
		md"🪳 **Hmm, not there yet.** Check your code for bugs and see if you can fix it."
	end
end

# ╔═╡ 863e3338-4767-4905-ba46-717d9923455c
md"""
Now let’s try applying `f` to values.

*Type* `f(1)` and `f(100)` *into the next two cells, then run them both.*
"""

# ╔═╡ b5aeaebc-9fb7-4cc3-992b-155b515e788a


# ╔═╡ 29e75ed5-7493-4430-a435-c1a0544601ff


# ╔═╡ 1ca3e15b-7fe3-4d62-8414-c524021436ed
md"""
You can also use an **array**, surrounded by square brackets, to find multiple values at once.

*Type* `[f(3), f(6), f(9)]` *into the cell below, then run the code.*
"""

# ╔═╡ d2aa4475-0d77-4a09-8d85-59cfeba00647


# ╔═╡ a31fff68-f915-4154-8ebc-0b8402322a6d
md"""
If you are in a hurry, you can use the `map` function to apply your functions to multiple arguments at once. For example, we can apply `f` to the inputs 0, 1, 100, and 6053 with the code

```julia
map(f, [0, 1, 100, 6053])
```

*Try out the code above in the cell below:*
"""

# ╔═╡ 3b47cd3f-4a6a-4db1-86c9-ba48599e7436


# ╔═╡ c922d7f7-30cd-4dd7-8e12-7e1dcd690785
md"""
In general,

```julia
map(f, [x_1, x_2, …, x_n]) = [f(x_1), f(x_2), …, f(x_n)]
```
"""

# ╔═╡ c8b7d846-5c71-4d97-8e67-049fd64a0a29
md"""
## Exercises
"""

# ╔═╡ fb88f4e5-9d6b-4b85-ab89-f272dc875a43
md"Solve each exercise by typing your code in the cell(s) immediately below it and pressing `Shift`+`Enter`. If you need more cells, click the '+' buttons on the left."

# ╔═╡ 01515ae7-6646-4ada-a1b1-f181dd5b61e3
md"How would you find the type of the value `6` in Julia?"

# ╔═╡ 89f1de57-280a-44f0-9f53-6358e067db0b


# ╔═╡ 1483a977-366d-4dd8-87f4-159ff9eb30f0
md"How would you find the type of the value `'6'` in Julia?"

# ╔═╡ db77c707-0d65-4e17-aac7-646405ce77f8


# ╔═╡ 4846f719-0d58-479e-b281-bc2778c47fcd
md"How would you find the type of the value `\"6\"` in Julia?"

# ╔═╡ ef25d019-509c-4732-bc1f-28c5b4a85e01


# ╔═╡ ae94bd19-97c9-4960-9839-2530cc9744f4
md"How would you compute $597 \times 1156$ in Julia?"

# ╔═╡ 520e3ce3-cb45-47f8-a5d8-a1f077acbf2b


# ╔═╡ ffe94a99-4743-4cb0-a68a-3ac7990907d5
md"How would you calculate $\sin(16°)$ in Julia?"

# ╔═╡ ee79b46f-4d82-4e11-9dd0-a86f6fae5dcd


# ╔═╡ 79e8493a-e7c5-4fc8-8517-dffbcea40d88
md"How would you calculate $\sin(\text{16 rad})$ in Julia?"

# ╔═╡ 56771478-4321-40c1-b030-19f7f2a68e5c


# ╔═╡ 61c1788f-e8d2-4659-82c9-e5eba88cf197
md"Define a function `cube` that raises its input to the power of 3."

# ╔═╡ 437f1b30-c800-4f15-aeb5-d4b6633f11d9


# ╔═╡ 0bc3c640-8f79-433e-8877-13fd6e7c303a
md"""
Apply `cube` to the numbers 1, 3, 5, 7, 9, and 11. (Hint: use `map` and `[1, 3, 5, 7, 9, 11]`.)
"""

# ╔═╡ ed7c7066-110e-4678-b808-b804dbda6171


# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.2"
manifest_format = "2.0"

[deps]
"""

# ╔═╡ Cell order:
# ╟─6fdddda2-3ac3-11ec-06b1-8b6384fa662e
# ╟─41d420f1-f4ed-4999-b1e9-543d7b7f0226
# ╟─7783e58b-c5fc-4e5d-8d9c-44a75f34b72f
# ╟─6e29e667-e4a8-465b-bc22-f3d0722569bc
# ╠═4bcd74b3-1937-4d16-aded-d70bdfb9c836
# ╟─cb4a0bcf-9c63-4b3e-b4fb-00a7f3236b10
# ╟─81294e2d-1bb6-4fb8-97f0-ed72bed0da10
# ╟─6fe5c559-d6dd-497f-b348-b30ed6734c24
# ╠═8d036cac-3811-4a51-a1c0-1a8617e4d3ad
# ╟─165dcae3-39f4-490b-b75f-087024f22db4
# ╟─24ac1846-6253-4f65-af6a-87ea1966c04d
# ╠═f5225f0d-bc5a-4c0b-8291-475405850ad6
# ╟─a65263b0-ce04-4343-9c95-b78b2a78094a
# ╟─a1dedb51-27f9-404b-84c6-ecf8ab7fc3bb
# ╠═5eca37d4-61da-4185-a5bb-c7beb17d9bd4
# ╟─5b7f7c58-f760-4dc0-bd39-ce1e78129187
# ╟─eae5d18f-a0b7-44c5-8d95-3b13074779c0
# ╟─fd3719d0-e20c-41bf-a3cb-078a7d74e585
# ╠═e64095c2-47d0-4cfe-8ecb-a550ee0b44f9
# ╟─ad18ebf1-b2d7-480f-8567-9403825b16ea
# ╟─b3181dd4-9143-44aa-8eb1-46226c8b4760
# ╠═a6bca36c-6eb2-4288-b9da-04b196b7e620
# ╟─e349215c-3215-4539-945e-e321ab632bb1
# ╠═8fb10aaa-339d-4fb2-b071-2a4c4e595251
# ╟─d9aa471c-1ca3-4695-badb-ab744d556e26
# ╟─d1a459ae-e0f4-4958-bc47-249227ba47e5
# ╠═b2618715-6bc6-48ba-a6a2-af03b07dc41a
# ╟─9796d4d2-ad68-4eee-8040-7cb512e4823d
# ╠═c11264e1-240e-4dd2-9e99-04ee3d1b6161
# ╟─1c9f6dec-6934-449f-92d9-a9ed485534cb
# ╠═a0cfd290-083c-4f1b-be31-5761df0cd547
# ╟─a025bf43-80de-43d4-bca9-b707aebac0f0
# ╠═c863dc6a-2db2-4cc4-a000-43295ac048b0
# ╟─0aae1a40-fc00-4bbb-afc4-8820aa3728d3
# ╟─946186ce-02ed-46cb-b229-23764d2e514e
# ╟─2311aa43-dffc-4d66-92f8-dd4b9ca7d001
# ╠═2451013b-f7cd-43e9-9c7c-1c6ce60a304c
# ╟─863e3338-4767-4905-ba46-717d9923455c
# ╠═b5aeaebc-9fb7-4cc3-992b-155b515e788a
# ╠═29e75ed5-7493-4430-a435-c1a0544601ff
# ╟─1ca3e15b-7fe3-4d62-8414-c524021436ed
# ╠═d2aa4475-0d77-4a09-8d85-59cfeba00647
# ╟─a31fff68-f915-4154-8ebc-0b8402322a6d
# ╠═3b47cd3f-4a6a-4db1-86c9-ba48599e7436
# ╠═c922d7f7-30cd-4dd7-8e12-7e1dcd690785
# ╟─c8b7d846-5c71-4d97-8e67-049fd64a0a29
# ╟─fb88f4e5-9d6b-4b85-ab89-f272dc875a43
# ╟─01515ae7-6646-4ada-a1b1-f181dd5b61e3
# ╠═89f1de57-280a-44f0-9f53-6358e067db0b
# ╟─1483a977-366d-4dd8-87f4-159ff9eb30f0
# ╠═db77c707-0d65-4e17-aac7-646405ce77f8
# ╟─4846f719-0d58-479e-b281-bc2778c47fcd
# ╠═ef25d019-509c-4732-bc1f-28c5b4a85e01
# ╟─ae94bd19-97c9-4960-9839-2530cc9744f4
# ╠═520e3ce3-cb45-47f8-a5d8-a1f077acbf2b
# ╟─ffe94a99-4743-4cb0-a68a-3ac7990907d5
# ╠═ee79b46f-4d82-4e11-9dd0-a86f6fae5dcd
# ╟─79e8493a-e7c5-4fc8-8517-dffbcea40d88
# ╠═56771478-4321-40c1-b030-19f7f2a68e5c
# ╟─61c1788f-e8d2-4659-82c9-e5eba88cf197
# ╠═437f1b30-c800-4f15-aeb5-d4b6633f11d9
# ╟─0bc3c640-8f79-433e-8877-13fd6e7c303a
# ╠═ed7c7066-110e-4678-b808-b804dbda6171
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
