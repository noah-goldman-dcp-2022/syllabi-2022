<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
  <meta charset="utf-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
    
  <title>Rational Numbers Project</title>
  <meta name="description" content="The summative for the Rational Numbers unit of
  MRWC."/>
  
  <link rel="stylesheet" href="/global-style.css"/>
  <link rel="stylesheet" href="/mrwc-style.css"/>
  <link rel="stylesheet" href="/exercises-style.css"/>
  
  <link rel="stylesheet"
    href="https://cdn.jsdelivr.net/npm/katex@0.13.13/dist/katex.min.css"
    integrity="sha384-RZU/ijkSsFbcmivfdRBQDtwuwVqK7GMOw6IMvKyeWL2K5UAlyp6WonmB8m7Jd0Hn"
    crossorigin="anonymous"/>
  <script type="module">
      import renderMathInElement from "https://cdn.jsdelivr.net/npm/katex@0.13.13/dist/contrib/auto-render.mjs"
      renderMathInElement(document.body, {
        delimiters: [
          {left: "\\(", right: "\\)", display: false},
          {left: "\\begin{equation}", right: "\\end{equation}", display: true},
          {left: "\\begin{align}", right: "\\end{align}", display: true},
          {left: "\\begin{align*}", right: "\\end{align*}", display: true},
          {left: "\\begin{alignat}", right: "\\end{alignat}", display: true},
          {left: "\\begin{gather}", right: "\\end{gather}", display: true},
          {left: "\\begin{CD}", right: "\\end{CD}", display: true},
          {left: "\\[", right: "\\]", display: true}
        ]
      })
  </script>
  
</head>

<body>

<p>
  <label for="student-name">Name:</label>
  <input name="student-name" id="student-name" type="text"/>
</p>
<p>
  <label for="date">Date started:</label>
  <input name="date-started" id="date-started" type="date" value="2022-09-19"/>
</p>
<header class="title-block">
  <h1>Rational Numbers Project</h1>
  <p class="subtitle">
    Unit Assessment | Rational Numbers Unit
  </p>
</header>

<main class="exercise-count-reset">
<section>
  <h2 id="instructions">Instructions</h2>
    <h3 id="rough-draft">Drafts</h3>
      <p>
        You will be writing two drafts for this project. 
      </p>
      <ol>
        <li>
          The <strong>rough draft</strong> is a chance for you to get feedback. I will look at your work and provide suggestions for you to succeed. The score on the rough draft does not factor into your grade; it’s just the score you’d receive on the project if you didn’t revise it.
        </li>
        <li>
          The <strong>final draft</strong> is what you will turn in. I will give you a grade according to the <a href="#success-criteria">success criteria</a> and <a href="#rubric">rubric</a>.
        </li>
      </ol>
      <p>
        I recommend doing most of the work on your <em>rough draft</em>. There is no difference between working sooner and later, except in stress level. Also, students tend to do much better when they get feedback and revise their projects.
      </p>
    <h3 id="policies">Policies</h3>
      <ul>
        <li>
          You may turn in a PDF, Google Doc, or picture of handwritten work for both drafts. Use the project template.
        </li>
        <li>
          You may use resources and collaborate, but the work you turn in must be your own (no direct copying or letting someone do it for you)!
        </li>
      </ul>
</section>
  <h2 id="problems">Problems</h2>
  <section>
    <h3 id="closure-and-arithmetic">Part 1: Closure and Arithmetic I</h3>
      <p>Answer in roughly 1-3 lines:</p>
      <ol class="exer-cises">
        <li>
          Explain, in an original sentence, the relationship between a number’s <a href="/mrwc/2-rational-numbers/lectures/01-the-rational-numbers#dfn-multiplicative-inverse">multiplicative inverse</a> and its <a href="/mrwc/2-rational-numbers/lectures/01-the-rational-numbers#dfn-reciprocal">reciprocal</a>.
        </li>
        <textarea id="inverse-sentence" name="inverse-sentence" rows="8" cols="72" ></textarea>
        <li>
          Find the <a href="/mrwc/2-rational-numbers/lectures/01-the-rational-numbers#dfn-multiplicative-inverse">multiplicative inverse</a> of \(\tfrac{83}{10}\). Show that your answer is correct, either with a calculation or a theorem from the notes.
        </li>
        <textarea id="mult-inverse" name="mult-inverse" rows="8" cols="72" ></textarea>
        <li>
          Consider the function \(f(x) = \dfrac{1-x}{1+x}\). By replacing \(x\) with an input (such as \(\frac{3}{4}\)) we can get an output:
          \begin{align*}
          f\left(\tfrac{3}{4}\right) &= \frac{1 - \tfrac{3}{4}}{1 + \tfrac{3}{4}}\\
            &= \frac{4 - 3}{4 + 3}\\
            &= \frac{1}{7}.
          \end{align*}
          <ol class="subproblems">
            <li>
              Find a formula for \(f(\frac{n}{d})\).
              <textarea id="f-formula" name="f-formula" rows="4" cols="72" ></textarea>
            </li>
            <li>
              There is an input for which \(f\) is undefined, and it’s not 0. What is this forbidden input?
              <textarea id="forbidden-x" name="forbidden-x" rows="3" cols="72" ></textarea>
            </li>
            <li>
              Prove that the <a href="/mrwc/2-rational-numbers/lectures/01-the-rational-numbers">rational numbers</a> are closed under \(f\). Your proof can start with “Let \(\tfrac{n}{d}\) be any rational except [answer to part b], where \(n\) and \(d \neq 0\) are integers.”
              <textarea id="closed-under-f" name="closed-under-f" rows="8" cols="72" ></textarea>
            </li>
          </ol>
        </li>
        
        <li>
          List 2-3 differences between the rational number system and the integer number system.
        <textarea id="advantages" name="advantages" rows="8" cols="72"></textarea>
        </li>
      </ol>
  </section>
  <section>
    <h3 id="fractions-on-lines">Part 2: Fractions on Lines</h3>
      <ol class="exer-cises">
        <li>For each number line, find the value of \(x\).
          <figure>
          <img role="img" src="/mrwc/2-rational-numbers/summative/number-line-01.svg" alt="Number line: first tick is 0, second tick is x, fifth tick is 1."/>
          <img role="img" src="/mrwc/2-rational-numbers/summative/number-line-02.svg" alt="Number line: first tick is 0, fourth tick is 1 over 3, fifth tick is x."/>
          <img role="img" src="/mrwc/2-rational-numbers/summative/number-line-03.svg" alt="Number line: second tick is negative 5 over 14, fourth tick is 5 over 14, sixth tick is x."/>
        </figure>
        </li>
      </ol>
  </section>
  <section>
    <h3 id="dig-off">Part 3: Dig-Off</h3>
      <p><em>Part 3 is about the following situation. Read the description closely.</em></p>
      <p>
        Renowned worker Ike Ulligan has an excavator. He thinks it is more powerful than a self-driving digger created by billionare Melon Husk. With each scoop, Ike’s machine can move \(\frac{1}{5}\) of a ton of sand, while Melon’s can move \(\frac{1}{7}\).</p>
      <p>
        Ike decides to go head-to-head with Melon: whoever’s machine can scoop more sand in a single DCP class period wins a huge amount of money. Over the course of the competition, Ike’s machine scoops 62 times, while Melon’s scoops 85 times.
      </p>
      <p>
        You are the referee for the dig-off. Your job is to perform calculations and decide who won. Unfortunately, cell phones and calculation devices are not allowed at the competition (they interfere with the speaker system), so you will have to rely on your “fraction sense”. Also, both teams will want to check your math, so you must explain your process.
      </p>
      <ol class="exer-cises">
        <li>
          Which machine moves more sand per scoop?
        </li>
        <textarea id="sand-per-scoop" name="sand-per-scoop" rows="8" cols="72" ></textarea>
        <li>
          How many tons of sand did each machine move during the competition? (Express your answer as a fraction – that is, in the form \(\frac{n}{d}\) where \(n\) and \(d \neq 0\) are integers.)
        </li>
        <textarea id="tons-of-sand" name="tons-of-sand" rows="8" cols="72" ></textarea>
        <li>
          Which machine moved more sand in the competition? (Since you’re avoiding calculators, explain how you’d compare both fractions to a nearby number.)
        </li>
        <textarea id="decide-winner" name="decide-winner" rows="4" cols="72" ></textarea>
        <li>
          Express your answers to #7 as <strong>exact</strong> decimals. For example, if you end up with \(13.518518518518...\) write it as \(13.\overline{518}\), <strong>not</strong> as \(13.519\) or \(13.52\). (You may use a calculator or <a href="https://wolframalpha.com" target="_blank">WolframAlpha</a> to check, but show the long division.)
        </li>
        <textarea id="decimal-calculation" name="decimal-calculation" rows="8" cols="72" ></textarea>
      </ol>
  </section>
  <section>
    <h3>Part 4: Positioning</h3>
    <ol class="exer-cises">
    <li>Estimate the positions of \(-\tfrac{4}{3}, -\tfrac{2}{3}, \tfrac{4}{3},\) and \(\tfrac{5}{3}\) on this number line. Label them.</li>
    <figure>
      <img role="img" src="/mrwc/2-rational-numbers/summative/number-line-estimate-01.svg" alt="Number line with 0 and 1."/>
    </figure>
      <li>
        Using a straightedge and the given grid (which may be on the next page):
        <ul>
          <li>Plot \(-\tfrac{4}{3}, -\tfrac{2}{3}, \tfrac{4}{3},\) and \(\tfrac{5}{3}\) on the dark line \(d = 1\).</li>
          <li>Label each point you plotted (just like how 1 is labelled).</li>
        </ul>
      </li>
      <figure>
        <img role="img" src="/mrwc/2-rational-numbers/summative/number-line-projection.svg"/>
      </figure>
      <li>
        Do the following:
        <ol class="subproblems">
          <li>Find a rational number between \(-\tfrac{4}{3}\) and \(-\tfrac{2}{3}\), a rational number between \(-\tfrac{2}{3}\) and \(\tfrac{4}{3}\), and a rational number between \(\tfrac{4}{3}\) and \(\tfrac{5}{3}\).</li>
          <li>Add your numbers to the diagram in #10 (using estimation).</li>
          <li>Add your numbers to the diagram in #11 (using a straightedge).</li>
        </ol>
        You may use the area below to write your numbers down:
        <textarea id="numbers-between" name="numbers-between" rows="5" cols="72" ></textarea>
      </li>
      <li>
        How many possible answers are there to the previous problem? Justify your response.
        <textarea id="numbers-between-2" name="numbers-between-2" rows="5" cols="72" ></textarea>
      </li>
    </ol>
</section>
<section>
  <h2 id="rubric">Rubric</h2>
    <table class="printable-table">
      <thead>
        <tr>
          <th>Criterion</th>
          <th>8: Strongly demonstrated</th>
          <th>7: Demonstrated</th>
          <th>6: Partly demonstrated</th>
          <th>5: Weakly demonstrated</th>
          <th>4: Not demonstrated</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Fraction sense</td>
          <td>
            You correctly compare and locate fractions. You choose efficient, elegant strategies whenever possible.
          </td>
          <td>
            You correctly compare and locate fractions most of the time. Sometimes you pick efficient/elegant strategies; other times, you pick ones that create extra work. 
          </td>
          <td>
            Your work shows some idea of how to compare and locate fractions. You choose strategies that work, but tend not to consider efficiency or elegance.
          </td>
          <td>
            You’re not really sure which fractions are where or why. You randomly choose strategies hoping one will work.
          </td>
          <td>Nothing to assess.</td>
        </tr>
        <tr>
          <td>Mathematical reasoning</td>
          <td>
            You spell your thinking out so clearly that any of your classmates could understand it. You describe every step of your process with words, data, pictures, or symbols. Your solution processes contain almost no errors.
          </td>
          <td>
            You spell your thinking out clearly enough that most of your classmates could understand it. You describe most steps with words, data, pictures, or symbols; a few steps are unclear. Your solution processes contain few or minor errors.
          </td>
          <td>
            Some classmates could understand what you did, with effort. You describe some steps with words, data, pictures, and symbols; other steps are unclear. You make significant errors in solving some problems.
          </td>
          <td>
            You either wrote just the answer or complete gobbledygook.
          </td>
          <td>
            Nothing to assess.
          </td>
        </tr>
        <tr>
          <td>Understanding of concepts</td>
          <td>Your work shows a deep understanding of the rational numbers.</td>
          <td>Your work shows a working understanding of the rational numbers.</td>
          <td>Your work shows a partial understanding of the rational numbers.</td>
          <td>Your work shows little understanding of the rational numbers.</td>
          <td>Nothing to assess.</td>
        </tr>
      </tbody>
    </table>
</section>
<section>
  <h2 id="success-criteria">Success criteria</h2>
    <p>In this project, I want to see that you can:</p>
    <ul>
      <li>Describe the motivation for expanding into the larger set of rational numbers (ℚ).</li>
      <li>Justify and apply the desirable properties of the rationals:</li>
        <ul>
          <li>
            ℚ is closed under addition, subtraction, and multiplication;
          </li>
          <li>
            The nonzero rational numbers are closed under division (unlike the nonzero integers);
          </li>
          <li>
            Every nonzero element of ℚ has a multiplicative inverse.
          </li>
        </ul>
      <li>
        Given a unit length on a number line, estimate the position of a rational number.
      </li>
      <li>
        Construct the exact position of a rational number with projection.
      </li>
      <li>
        Use projection to gain insights about rational numbers.
      </li>
      <li>
        Compare fractions using fraction sense.
      </li>
      <li>
        Convert between fractions and decimals.
      </li>
      <li>
        Understand the connection between fraction and decimal representations of a rational number.
      </li>
    </ul>
</section>
</main>

</body>
</html>
