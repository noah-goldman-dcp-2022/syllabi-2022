<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
  <meta charset="utf-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
    
  <title>The Rational Numbers</title>
  <meta name="description" content="An introduction to the rational numbers – why they exist,
    how to calculate with them, and so on."/>
  
  <link rel="stylesheet" href="/mrwc/mrwc-lecture-style.css"/>
  
  <link rel="stylesheet"
    href="https://cdn.jsdelivr.net/npm/katex@0.13.13/dist/katex.min.css"
    integrity="sha384-RZU/ijkSsFbcmivfdRBQDtwuwVqK7GMOw6IMvKyeWL2K5UAlyp6WonmB8m7Jd0Hn"
    crossorigin="anonymous"/>
  <script type="module">
      import renderMathInElement from "https://cdn.jsdelivr.net/npm/katex@0.13.13/dist/contrib/auto-render.mjs"
      renderMathInElement(document.body, {
        delimiters: [
          {left: "\\(", right: "\\)", display: false},
          {left: "\\begin{equation}", right: "\\end{equation}", display: true},
          {left: "\\begin{align}", right: "\\end{align}", display: true},
          {left: "\\begin{align*}", right: "\\end{align*}", display: true},
          {left: "\\begin{alignat}", right: "\\end{alignat}", display: true},
          {left: "\\begin{gather}", right: "\\end{gather}", display: true},
          {left: "\\begin{CD}", right: "\\end{CD}", display: true},
          {left: "\\[", right: "\\]", display: true}
        ]
      })
  </script>
  
</head>

<body>

<header class="title-block">
  <h1>The Rational Numbers</h1>
  <p class="subtitle">
    Lecture Notes | Rational Numbers Lesson 1
  </p>
</header>

<section>
  <h2 id="learning-outcomes">Learning outcomes</h2>
    <p>You will…</p>
    <ol>
      <li>Describe the motivation for expanding into the larger set of rational numbers.</li>
      <li>Justify and apply the fact that multiplicative inverses exist on the rationals.</li>
    </ol>
</section>

<section>
  <h2 id="key-vocab">Key vocabulary</h2>
  <ul>
    <li>
      <a href="#dfn-rational-number">Rational number</a> / <a href="#dfn-Q">ℚ</a>
    </li>
    <li>
      <a href="#dfn-multiplicative-inverse">Multiplicative inverse</a> / <a href="#dfn-reciprocal">Reciprocal</a> 
    </li>
  </ul>
</section>
<main>
<section>
  <h2 id="motivation">Motivation</h2>
  <p>
    We’ve just spent a whole unit talking about closure. We saw that the natural numbers are not closed under subtraction. So we created the set of integers, ℤ. And we saw that the set of integers is closed under a lot of things: addition, subtraction, and multiplication.
  </p>
  <p>
    But it’s <em>not</em> closed under division. If we want to extend division to all pairs of integers (without dividing by 0) we need a larger set of outputs. This is where fractions come in.
  </p>
  <p>
    Another way of thinking about division: When I write \(2 \div 3\), I’m asking a question. What number times 3 gives me 2? Or, in equation form, I want the solution to: 
  </p>
  \[3x = 2\]
  <p>
    Let’s plot the lines \(y = 3x\) and \(y = 2\) to see where they intersect.
  </p>
  <object type="image/svg+xml" data="/mrwc/2-rational-numbers/lectures/01-the-rational-numbers/intersect_plot.svg">
    A graph of the lines \(y = 3x\) and \(y = 2\) with their intersection at \((\frac{2}{3}, 2)\).
  </object>
  <p>
    The intersection appears to be somewhere between \(x = 0\) and \(x = 1\). This means \(\frac{2}{3}\) cannot be an integer, as there are no integers strictly between 0 and 1.
  </p>
</section>
<section>
  <h2 id="the-rational-numbers">The rational numbers</h2>
  <p>
    Let’s expand our number system so that we account for all these fractions.
  </p>
  <section class="defi-nition">
    <h3 class="label">Definition: Rational number</h3>
    <p>
      A <dfn id="dfn-rational-number">rational number</dfn> is a number that can be represented as a <em>ratio</em> or <em>quotient</em> of two integers. Rational numbers can be represented as \(\frac{n}{d}\) where \(n\) and \(d\) are integers and \(d \neq 0\).
    </p>
    <p>
      The <dfn id="dfn-Q">set of rational numbers</dfn> is written ℚ (for “quotient”):
    </p>
    <p>\[\mathbb{Q} = \left\{ \frac{n}{d} : n, d \text{ integers, } d \neq 0 \right\}\]</p>
  </section>
  <p>In more casual terms,</p>
    \begin{equation}
      \text{rational number} = \frac{\text{some integer}}{\text{some nonzero integer}}.
    \end{equation}
  <section class="ex-ample">
    <h3 class="label">Example: The integers</h3>
    <p>
      Every integer is also a rational number. That’s because every integer \(n\) can be expressed as the ratio \(\frac{n}{1}\). For example, \(2 = \frac{2}{1}\), \(3 = \frac{3}{1}\), \(-4 = \frac{-4}{1}\), and so on.
    </p>
  </section>
</section>

<section>
  <h2 id="multiplicative-inverses-on-the-rational-numbers">Existence of multiplicative inverses</h2>
  <p>
    One special case of division is the taking of multiplicative inverses.
  </p>
  <section class="defi-nition">
    <h3 class="label">Definition: Multiplicative inverse</h3>
      <p>
        Two numbers \(a\) and \(b\) are <dfn id="dfn-multiplicative-inverse">multiplicative inverses</dfn> of each other iff \(ab = ba = 1\). In this case, we write \(a = b^{-1}\) or \(a = \frac{1}{b}\).
      </p>
  </section>
  <p>
    We didn’t have multiplicative inverses over the integers – at least not for every number. There is no integer that, when multiplied by 3, gives 1. But it turns out that every rational number has a multiplicative inverse equal to its <em>reciprocal</em>.
  </p>
  <section class="defi-nition">
    <h3 class="label">Definition: Reciprocal</h3>
    <p>
      Let \(\frac{n}{d}\) be a nonzero fraction. The <dfn id="dfn-reciprocal">reciprocal</dfn> of \(\frac{n}{d}\) is \(\frac{d}{n}\).
    </p>
  </section>
  <section class="theo-rem">
    <h3 class="label">Theorem: Mult. inverse = Reciprocal</h3>
      <p>
        Let \(\frac{n}{d}\) be a nonzero rational number. Then \(\frac{n}{d}\) has exactly one multiplicative inverse, which is equal to its reciprocal.
      </p>
      <h4 class="proof_start">Proof.</h4>
      <p>To show \(\frac{n}{d}\) and \(\frac{d}{n}\) are multiplicative inverses, all we need to show is that they multiply to give 1:</p>
        \[\frac{n}{d} \; \frac{d}{n} = \frac{nd}{dn} = \frac{nd}{nd} = 1.\]
      <p>
        (The rest of the proof is not necessary for understanding.) To show that \(\frac{d}{n}\) is the <em>only</em> mult. inverse, suppose \(r\) is any inverse of \(\frac{n}{d}\). Then
      </p>
      <div>
        \begin{align*}
          r \times \frac{n}{d} &= 1;\\
          \left(r \times \frac{n}{d}\right) \times \frac{d}{n} &= 1 \times \frac{d}{n};\\
          r \times \left( \frac{n}{d} \times \frac{d}{n} \right) &= \frac{d}{n};\\
          r \times 1 &= \frac{d}{n};\\
          r &= \frac{d}{n}.
        \end{align*}
      </div>
      <p>Thus, the only mult. inverse of \(\frac{n}{d}\) is \(\frac{d}{n}\). ∎</p>
  </section>
</section>

</main>

</body>
</html>
