<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
  <meta charset="utf-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
    
  <title>Proof Strategies</title>
  <meta name="description" content="In less than 140 characters:
    what is on this page, and how does it differ from other pages?"/>
  
  <link rel="stylesheet" href="/mrwc/mrwc-lecture-style.css"/>
  
  <link rel="stylesheet"
    href="https://cdn.jsdelivr.net/npm/katex@0.13.13/dist/katex.min.css"
    integrity="sha384-RZU/ijkSsFbcmivfdRBQDtwuwVqK7GMOw6IMvKyeWL2K5UAlyp6WonmB8m7Jd0Hn"
    crossorigin="anonymous"/>
  <script type="module">
    import renderMathInElement from "https://cdn.jsdelivr.net/npm/katex@0.13.13/dist/contrib/auto-render.mjs";
    renderMathInElement(document.body);
  </script>
  
</head>

<body>

<header class="title-block">
  <h1>Proof Strategies</h1>
  <p class="subtitle">
    Handout | Integers Lesson 5
  </p>
</header>

<section>
  <h2 id="learning-outcomes">Learning outcomes</h2>
    <p>You will be able to…</p>
    <ol>
      <li>Restate claims and assumptions by unwinding technical definitions of key terms.</li>
      <li>Translate word statements into symbols.</li>
      <li>Perform direct proofs by assuming the antecedent.</li>
    </ol>
</section>

<section>
  <h2 id="list-of-strategies">List of strategies</h2>
  <ol>
    <li>
      <a href="#unwind-the-definitions">Unwind the definitions.</a>
    </li>
    <li>
      <a href="#define-your-own-symbols">Define your own symbols.</a>
    </li>
    <li>
      <a href="#direct-proof">Direct proof (a.k.a. assume the antecedent).</a>
    </li>
  </ol>
</section>

<main>
<section>
  <h2 id="unwind-the-definitions">Strategy 1: Unwind the definitions.</h2>

  <p>This is how you get started. Look at the claim you are supposed to prove. Does it contain a concept we’ve defined in class? <strong>Write out the definition.</strong></p>

  <p>Look at the assumptions you’ve been given. Do they contain concepts we’ve defined? <strong>Write out their definitions and known properties.</strong></p>

  <section class="ex-ample">
    <h3 class="label">Example: Integers > 3</h3>
    <p>Suppose we are asked to do the following.</p>

        <blockquote>Let \(S\) be the set of <a href="https://nsg-teaching.onrender.com/mrwc/1-integers/lectures/integers-04-closure-infinite-sets#dfn-integer" target="_blank">integers</a> greater than 3. Prove or disprove: \(S\) is <a href="https://nsg-teaching.onrender.com/mrwc/1-integers/lectures/integers-03-closure/#dfn-closure" target="_blank">closed</a> under addition.</blockquote>

        <p>Just looking at this, what phrases seem to be important? “Closed under addition” seems like an important term, so let’s revisit <a href="https://nsg-teaching.onrender.com/mrwc/1-integers/lectures/integers-03-closure/#dfn-closure" target="_blank">the definition of closure</a> and unwind it. Our problem then becomes (highlighted means changed):</p>

        <blockquote>Let \(S\) be the set of integers greater than 3. Prove or disprove: <span class="hl-change">If addition is applied to elements of \(S\), the outputs must also be in \(S\).</span></blockquote>

        <p>We can go further by unwinding “elements of \(S\)” using the definition of \(S\). Remember, “element of \(S\)” means “integer greater than 3” in this problem. So our task is (highlighted means changed):</p>

        <blockquote>Prove or disprove: If addition is applied to <span class="hl-change">integers greater than 3</span>, the outputs must also be <span class="hl-change">integers greater than 3</span>.</blockquote>

        <p>The problem has now become much easier to understand, because we’ve broken down some of the academic language.</p>
  </section>
</section>

<section>
  <h2 id="define-your-own-symbols">Strategy 2: Define your own symbols.</h2>

  <p>Despite all the grumbling, angst, and whining I hear about abstract math symbols, the reality is <strong>variables save you time and work.</strong> Translating statements into symbols allows you to use algebra – among math’s most powerful tools. And symbols are the best-known way to deal with infinite sets.</p>

  <section class="ex-ample">
    <h3 class="label">Example: Integers > 3</h3>

      <p>The problem from before, as we left it, was:</p>

      <blockquote>Prove or disprove: If addition is applied to integers greater than 3, the outputs must also be integers greater than 3.</blockquote>

      <p>That’s a lot of words, and algebra doesn’t handle words very well. Let’s translate to symbols.</p>

      <blockquote>Let \(m\) and \(n\) be integers. Prove or disprove: If \(m > 3\) and \(n > 3\), then \(m + n > 3\).</blockquote>

      <p>Notice that we had to <em>define</em> \(m\) and \(n\) before using them. Now we have some inequalities, \(m > 3\) and \(n > 3\), which we can combine more easily.</p>
  </section>
</section>

<section>
  <h2 id="direct-proof">Strategy 3: Direct proof</h2>

  <p>Many statements we wish to prove take the form of “if-then” statements. Often, you can prove an “if-then” statement as follows:</p>

    <ol>
      <li>Assume the “if” part (or <em>antecedent</em>) is true.</li>
      <li>Show that the “then” part (or <em>consequent</em>) must be true.</li>
    </ol>

  <section class="ex-ample">
    <h3 class="label">Example: Integers > 3</h3>

    <p>In its original form, the problem we wanted to solve was:</p>

    <blockquote>Let \(S\) be the set of integers greater than 3. Prove or disprove: \(S\) is closed under addition.</blockquote>

    <p>We turned it into the “if-then” statement:</p>

      <blockquote>Let \(S\) be the set of integers greater than 3. <strong>If</strong> \(m\) and \(n\) belong to \(S\), <strong>then</strong> so does \(m + n\).</blockquote>

    <p>Here’s how we do it.</p>

    <h4>Proof</h4>
      <p>
        Suppose \(m\) and \(n\) belong to \(S\). Then \(m\) and \(n\) are integers such that \(m > 3\) and \(n > 3\). We need to show two things:
      </p>
      <ul>
        <li>That \(m + n\) is an integer, and</li>
        <li>That \(m + n > 3\).</li>
      </ul>
      <p>We know the sum \(m+n\) is an integer because \(\mathbb{Z}\) is closed under addition.</p>
      <p>Adding the inequalities \(m > 3\) and \(n > 3\), we get \(m + n > 3 + 3\).</p>
      <p>Clearly this implies \(m + n > 3\). Since \(S\) consists of all integers greater than 3, we know \(m + n\) belongs to \(S\). ∎</p>
  </section>
</section>

</main>

</body>
</html>
