<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
  <meta charset="utf-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
    
  <title>Closure on Infinite Sets (Lecture Notes | Integers Lesson 4)</title>
  <meta name="description" content="In less than 140 characters:
    what is on this page, and how does it differ from other pages?"/>

  <link rel="stylesheet" href="/mrwc/mrwc-lecture-style.css"/>
  <link rel="stylesheet"
    href="https://cdn.jsdelivr.net/npm/katex@0.13.13/dist/katex.min.css"
    integrity="sha384-RZU/ijkSsFbcmivfdRBQDtwuwVqK7GMOw6IMvKyeWL2K5UAlyp6WonmB8m7Jd0Hn"
    crossorigin="anonymous"/>
  <script type="module">
    import renderMathInElement from "https://cdn.jsdelivr.net/npm/katex@0.13.13/dist/contrib/auto-render.mjs";
    renderMathInElement(document.body);
  </script>
  
</head>

<body>

<header class="title-block">
  <h1>Closure on Infinite Sets</h1>
  <p class="subtitle">
    Lecture Notes | Integers Lesson 4
  </p>
</header>

<section>
  <h2 id="learning-outcomes">Learning outcomes</h2>
    <p>You will…</p>
    <ol>
      <li>Make and test conjectures about the closure of an <em>infinite</em> set under an operation.</li>
      <li>Understand that closure is first an organizing principle for understanding sets and operations.</li>
    </ol>
</section>

<section>
  <h2 id="key-vocab">Key vocabulary</h2>
  <ul>
    <li>
      <a href="#dfn-natural-number">Natural number</a>
    </li>
    <li>
      <a href="#dfn-whole-number">Whole number</a>
    </li>
    <li>
      <a href="#dfn-integer">Integer</a>
    </li>
    <li>
      <a href="#dfn-even">Even</a>
    </li>
    <li>
      <a href="#dfn-integer-multiple">Integer multiple</a>
    </li>
  </ul>
</section>

<main>

<section>
  <h2 id="natural-numbers-review">Natural numbers review</h2>
  <p>Last time, we discussed closure on <em>finite</em> sets. Now we are ready to move on to infinite ones. Let’s (re-)meet some of the most important infinite sets:</p>
  <section class="defi-nition">
    <h3 class="label">Definition: Natural and whole numbers</h3>
    <p>
      The set of <dfn id="dfn-natural-number">natural numbers</dfn> is given by:
      \[\mathbb{N} = \{1, 2, 3, 4, 5, 6, \dotsc\} \]
    </p>
    <p>
      The set of <dfn id="dfn-whole-number">whole numbers</dfn> is given by:
      \[\mathbb{W} = \{0, 1, 2, 3, 4, 5, \dotsc\}\]
    </p>
  </section>
  <p>Convince yourself that both these sets are closed under addition and multiplication.</p>
</section>
<section>
  <h2 id="idea-of-the-integers">Commercial for the integers</h2>
    <blockquote>
      “The true mathematician takes ‘you can’t do that’ as a challenge.” —<a href="https://www.youtube.com/watch?v=TINfzxSnnIE">Vi Hart</a>
    </blockquote>
    <p>
      Has this ever happened to you? You’re trying to take a bigger (natural) number from a smaller one. But the natural and whole numbers aren’t <em>closed</em> under subtraction!
    </p>
    <p>
      Well folks, now there’s hope. Just stick a negative sign on some of your numbers, and you can subtract them however you want!
    </p>
  <section class="defi-nition">
    <h3 class="label">Definition: Integers</h3>
    <p>
      The set of <dfn id="dfn-integer">integers</dfn> is given by:
      \[\mathbb{Z} =  \{\dotsc, -3, -2, -1, 0, 1, 2, 3, \dotsc\}\]
    </p>
  </section>
</section>

<section>
<h2 id="informal-closure-on-infinite-sets">Informal closure on infinite sets</h2>
  <section class="ex-ample">
    <h3 class="label">Discussion: Closure of the integers</h3>
    
    <p>Answer the following questions, and explain:</p>

    <ol>
      <li>Is the set of integers, \(\mathbb{Z}\), closed under addition?</li>
      <li>Is \(\mathbb{Z}\) closed under subtraction?</li>
      <li>Is \(\mathbb{Z}\) closed under multiplication?</li>
      <li>Is the set of nonzero integers closed under division?</li>
    </ol>

    <aside>(You don’t have to formally prove… yet. You should consider positive numbers, negative numbers, and zero when applicable.)</aside>
  </section>
</section>

<section>
  <h2 id="formal-closure-on-infinite-sets">Formal closure on infinite sets</h2>
    <section class="ex-ample">
      <h3 class="label">Discussion: What is an even integer?</h3>
      <p>Try to define an “even integer” and see what you come up with.</p>
    </section>

    <p>We prefer this definition:</p>

    <section class="defi-nition">
      <h3 class="label">Definition: Even</h3>
      <p>
        An integer \(m\) is <dfn id="dfn-even">even</dfn> <abbr title="if and only if">iff</abbr> \(m = 2k\) for some integer \(k\).
      </p>
    </section>

    Another way of saying “even” is “integer multiple of 2”. Likewise, multiples of 3 can be written as \(3k\) for some integer \(k\), and so on.

    <section class="defi-nition">
      <h3 class="label">Definition: Integer multiple</h3>
      <p>
        Let \(m\) and \(n\) be integers. We say \(m\) is an <dfn id="dfn-integer-multiple">integer multiple</dfn> of \(n\) <abbr title="if and only if">iff</abbr> \(m = nk\) for some integer \(k\).
      </p>
    </section>

    <p>Now for our first closure proof.</p>

    <section class="ex-ample">
      <h3 class="label">Example: Even integers under +</h3>
      <p>Let \(S\) be the set of all even integers (multiples of 2). Prove or disprove: \(S\) is closed under addition.</p>

      <p>(Ask students to try this first.)</p>

      <p>
        <em>Proof.</em> Let \(a\) and \(b\) be even integers. Then \(a = 2j\) and \(b = 2k\) for some integers \(j\) and \(k\). So we have:
        \[a + b = 2j + 2k = 2(j + k).\]
        But since the integers are closed under addition, \(j + k\) is an integer. So \(2(j + k)\) must be even.
      </p>
    </section>
</section>

</main>

</body>
</html>
