<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
  <meta charset="utf-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
    
  <title>Closure Proof Project</title>
  <meta name="description" content="The summative assessment for the Integers unit."/>
  
  <link rel="stylesheet" href="/mrwc/mrwc-lecture-style.css"/>
  
  <link rel="stylesheet"
    href="https://cdn.jsdelivr.net/npm/katex@0.13.13/dist/katex.min.css"
    integrity="sha384-RZU/ijkSsFbcmivfdRBQDtwuwVqK7GMOw6IMvKyeWL2K5UAlyp6WonmB8m7Jd0Hn"
    crossorigin="anonymous"/>
  <script type="module">
      import renderMathInElement from "https://cdn.jsdelivr.net/npm/katex@0.13.13/dist/contrib/auto-render.mjs";
      renderMathInElement(document.body);
  </script>
  
</head>

<body>

<header class="title-block">
  <h1>Closure Proof Project</h1>
  <p class="subtitle">
    Summative | Integers Unit
  </p>
</header>

<section>
  <h2 id="instructions">Instructions</h2>
  <p>
    Answer these questions on a separate paper.
  </p>
    <h3 id="ascii">Part 1: Computer Science</h3>

      <ol>
        <li>
          Let \(U\) = {<code>"a"</code>, <code>"e"</code>, <code>"i"</code>, <code>"o"</code>, <code>"u"</code>}. Show that \(U\) is <strong>not</strong> closed under <a href="/mrwc/1-integers/lectures/integers-03-closure#dfn-concatenate">concatenation</a>. Explain why your counterexample demonstrates \(U\) is not closed.
        </li>
        <li>
          Let \(V\) be the set of all strings made of the characters <code>'a'</code>, <code>'e'</code>, <code>'i'</code>, <code>'o'</code>, and <code>'u'</code>. For example, <code>"aooiae"</code>, <code>"uueoa"</code>, and <code>"eeeeeeeeeeeeeeeee"</code> are elements of \(V\). Explain (no need for formal proof) why this set <strong>is</strong> closed under concatenation.
        </li>
        <li>
          Can a (nonempty) finite set of strings be closed under concatenation? Why or why not?
        </li>
      </ol>

    <h3 id="married-couples">Part 2: Married Couples of Infinite Sets</h3>
      <p>Choose <strong>one</strong> pair of sets from the following list:</p>

      <dl>
        <dt>Pair A: Composites</dt>
        <dd>
          Let \(S_1\) be the set of composite <a href="/mrwc/1-integers/lectures/integers-04-closure-infinite-sets#dfn-whole-number">whole numbers</a> less than 250.
          Let \(S_2\) be the set of composite whole numbers greater than or equal to 250.
        </dd>

        <dt>Pair B: Modulo 6</dt>
        <dd>
          Let \(S_1\) be the set of integers of the form \(6n\) where \(n\) is a natural number. Let \(S_2\) be the set of integers of the form \(6n - 1\) where \(n\) is a natural number.
        </dd>

        <dt>Pair C: Minecraft cubes</dt>
        <dd>
          Let \(S_1\) be the set of numbers of the form \(3n^3\), where \(n\) is an integer. Let \(S_2\) be the set of integers of the form \(n^3\), where \(n\) is an integer.
        </dd>
      </dl>

      <p>For the pair you chose:</p>

        <ol start="4">
          <li>List five distinct elements of each set.</li>
          <li>Find a counterexample to show that one of the sets is not closed under multiplication. Explain why your counterexample demonstrates the set is not closed.</li>
          <li>Write a <a href="/mrwc/1-integers/lectures/integers-05-proof-strategies">formal proof</a> that the other set is closed under multiplication.</li>
        </ol>

    <h3 id="mind-over-multiplication">Part 3: Mind Over Multiplication</h3>

    <ol start="7">
      <li>
        Write out two different ways you might calculate \((192 - 168)(1111 + 99)\) in your head. Only include steps that a typical classmate would actually take.
      </li>
    </ol>
</section>

<section>
  <h2 id="success-criteria">Success criteria</h2>
    <h3 id="objectives">Objectives</h3>
      <p>In this project, you will show that you’ve learned how to…</p>
      <ol>
        <li>Fluently perform basic calculations with integers without the help of technology.</li>
        <li>Understand that closure is first an organizing principle for understanding sets and operations.</li>
        <li>Make and test conjectures about the closure of a <em>finite</em> set under an operation.</li>
        <li>Make and test conjectures about the closure of an <em>infinite</em> set under an operation.</li>
        <li>Calculate fluently with the order of operations.</li>
        <li>Recognize the importance of context in mathematics.</li>
        <li>Avoid over-generalizing statements to situations where they don’t apply.</li>
      </ol>
      <p>Any skills you haven’t yet mastered can be picked up while doing the project.</p>
    <h3 id="rubric">Rubric</h3>
      <table>
        <thead>
          <tr>
            <th>Criterion</th>
            <th>8: Strongly demonstrated</th>
            <th>7: Demonstrated</th>
            <th>6: Partly demonstrated</th>
            <th>5: Weakly demonstrated</th>
            <th>4: Not demonstrated</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <strong>Understanding of concepts</strong><br />
              <p>Does your project show that you understand integers, primes, sets, and closure?</p>
            </td>
            <td>
              Your work shows a deep understanding of integers, primes, sets, and closure.
            </td>
            <td>
              Your work shows a working understanding of integers, primes, sets, and closure.
            </td>
            <td>
              Your work shows a partial understanding of integers, primes, sets, and closure.
            </td>
            <td>
              Your work shows little understanding of integers, primes, sets, and closure.
            </td>
            <td>
              Nothing to assess.
            </td>
          </tr>
          <tr>
            <td>
              <strong>Proofs</strong><br />
              <p>Does your project show that you can write a formal proof?</p>
            </td>
            <td>
              Each proof clearly contains a chain of reasoning from assumptions to conclusion. All steps in your proofs are correct. Everything you write either furthers the argument or clarifies the situation for your reader. You deeply understand what a proof is and do not confuse it with showing examples.
            </td>
            <td>
              Your proofs contain chains of reasoning from assumptions to conclusion. Most steps in your proofs are correct. Most of what you write either furthers the argument or clarifies the situation for your reader. You understand what a proof is and do not confuse it with showing examples.
            </td>
            <td>
              Your proofs sometimes contain chains of reasoning from assumptions to conclusion. Most steps in your proofs are correct, but there is some confusion between a proof and showing examples. However, there are enough elements that are proof-like to get the point across.
            </td>
            <td>
              You only present informal arguments, not proofs.
            </td>
            <td>
              Nothing to assess.
            </td>
          </tr>
          <tr>
            <td>
              <strong>Calculation fluency</strong><br />
              <p>Does your project show that you can calculate with integers strategically and effectively?</p>
            </td>
            <td>
              Your calculations are correct and spelled out clearly for the reader. You consistently prefer efficient strategies when less-efficient ones were available.
            </td>
            <td>
              Your calculations are mostly correct and spelled out. You chose strategies with some regard to efficiency.
            </td>
            <td>
              Your calculations contain some errors but are spelled out with medium clarity. You did not attempt to choose the best strategies, but chose ones that worked.
            </td>
            <td>
              Your calculations contain many errors and/or are hard to understand.
            </td>
            <td>
              Nothing to assess.
            </td>
          </tr>
        </tbody>
      </table>
</section>

</body>
</html>
