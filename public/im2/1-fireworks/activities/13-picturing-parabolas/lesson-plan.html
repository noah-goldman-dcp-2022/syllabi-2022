<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
  <meta charset="utf-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
    
  <title>Picturing Parabolas (Lesson Plan)</title>
  <meta name="description" content="In less than 140 characters:
    what is on this page, and how does it differ from other pages?"/>
  
  <link rel="stylesheet" href="/im2/im2-lecture-style.css"/>
  <link rel="stylesheet" href="/im2/1-fireworks/activities/13-picturing-parabolas/13-picturing-parabolas.css"/>
  
  <link rel="stylesheet"
    href="https://cdn.jsdelivr.net/npm/katex@0.13.13/dist/katex.min.css"
    integrity="sha384-RZU/ijkSsFbcmivfdRBQDtwuwVqK7GMOw6IMvKyeWL2K5UAlyp6WonmB8m7Jd0Hn"
    crossorigin="anonymous"/>
  <script type="module">
      import renderMathInElement from "https://cdn.jsdelivr.net/npm/katex@0.13.13/dist/contrib/auto-render.mjs";
      renderMathInElement(document.body, {
        delimiters: [
          {left: "\\(", right: "\\)", display: false},
          {left: "\\begin{equation}", right: "\\end{equation}", display: true},
          {left: "\\begin{align}", right: "\\end{align}", display: true},
          {left: "\\begin{align*}", right: "\\end{align*}", display: true},
          {left: "\\begin{alignat}", right: "\\end{alignat}", display: true},
          {left: "\\begin{gather}", right: "\\end{gather}", display: true},
          {left: "\\begin{CD}", right: "\\end{CD}", display: true},
          {left: "\\[", right: "\\]", display: true}
        ]
      })
  </script>
  <style>
    .blank-word {
      --blank-word-color: var(--course-color);
    }
  </style>
  
</head>

<body>

<header class="title-block">
  <h1>Picturing Parabolas</h1>
  <p class="subtitle">
    Lesson Plan | Fireworks Lesson 13
  </p>
</header>

<main>
<section>
  <h2 id="learning-outcomes">Learning outcomes</h2>
    <p>Students will…</p>
    <ol>
      <li>Distinguish between vertex form and standard form.</li>
      <li>Identify key features of parabolas based on their vertex form equation, including:</li>
      <ul>
        <li>Location of the vertex;</li>
        <li>Whether the function has a minimum or maximum;</li>
        <li>Whether the curve opens upward or downward.</li>
      </ul>
    </ol>
</section>
<section>
  <h2 id="before-class">Before class…</h2>
    <ol class="teacher-only">
      <li>
        Print one copy of the student activity for each table group (or however you group the students).
      </li>
      <li>
        Draw a large version of the diagram on a sheet of poster paper. You can draw it in marker (not pencil) and leave space for the vocab, but <em>do not write the vocab on it!</em>
      </li>
      <li>
        Instead, write each vocab term on a sticky note or index card. You will be pasting these on the diagram during each period (and taking them off if you teach IM2 in the same room later).
      </li>
    </ol>
</section>
<section>
  <h2 id="kickoff-activity">Part 1: Kickoff activity (3-5 min.)</h2>
    <p class="teacher-only">
      Give students 3-5 minutes to try this activity on their own. Walk around the room and make a note of any useful discussions you might want to share with the whole class. Tell these groups you will ask them to share later.
    </p>
    <p>
      Let’s take a step back and make sure we understand how this unit’s vocabulary connects to the equations and graphs.
    </p>
    <p>
      Below, I have graphed a parabola and written its equation in two different ways. Fill in the blank boxes on this diagram using the terms below:
    </p>
    <ul class="word-bank">
      <li>Standard form</li>
      <li>Vertex form</li>
      <li>Vertex</li>
      <li><em>x</em>-intercepts</li>
      <li><em>a</em></li>
      <li><em>h</em></li>
      <li><em>k</em></li>
      <li>Parabola</li>
    </ul>
    <img src="https://nsg-teaching.onrender.com/im2/1-fireworks/activities/13-picturing-parabolas/Picturing_Parabolas_answer_key_opt.svg"
      class="fill-blank-activity"
      alt="Match the quadratics vocabulary with the appropriate part of the image."/>

    <p>Which term did you not use? Is there a way you might add it to the diagram?</p>
    
    <p>Is the vertex a minimum or a maximum?</p>

    <p class="teacher-only">Minimum</p>
</section>
<section>
  <h2 id="whole-class">Part 2: Whole class (10 min.)</h2>
    <div class="teacher-only">
    <p>
      Go through the poster-paper version of the diagram with the class. Use the sticky notes or index cards to label the parts of the diagram. As you go, ask groups that had a particularly fruitful discussion about a term to help you with that specific term. If students were stuck and you didn’t hear much fruitful discussion, just plow on ahead.
    </p>
    <p>Key points, which may be worth writing on the diagram (not sure how you’d do that with multiple periods):</p>
    <ul>
      <li>A parabola’s <em>x</em>-intercepts are located at the “roots” or “zeros” of the underlying quadratic.</li>
      <li>At the <em>x</em>-intercepts of any curve, \(y = 0\).</li>
      <li>The <em>x</em>-coordinate of the vertex is <em>h</em>, and the <em>y</em>-coordinate of the vertex is <em>k</em>.</li>
    </ul>
    </div>
</section>
<section>
  <h2 id="within-group-discussion">Part 3: Within-group discussion (20 min.)</h2>
    <p>
      <strong>Don’t start this part of the activity until your teacher says it’s time to do so!</strong> Discuss the following questions with your group:
    </p>
    <ol>
      <li>
        It is most useful to put the equation in vertex form to find the <span class="blank-word">vertex</span> or the <span class="blank-word"><em>x</em>-intercepts</span>.
      </li>
      <li>
        If \(a &lt; 0\) (negative), the parabola <span class="blank-word">opens downward</span>. If \(a &gt; 0\) (positive), the parabola <span class="blank-word">opens upward</span>.
      </li>
      <li>
        Completing the square means converting from <span class="blank-word">standard</span> form to <span class="blank-word">vertex</span> form.
      </li>
      <li>
        For the equation \(y = (x + 3)(x - 7)\):
        <ol type="a">
          <li>How many ways can you think of to rewrite the right hand side? <span class="blank-word">\(y = (x - 2)^2 - 25 = x^2 - 4x - 21\)</span></li>
          <li>How many ways can you find the \(x\)-intercepts?</li>
          <li>Sketch a graph of the equation.</li>
        </ol>
      </li>
    </ol>
</section>
</main>

</body>
</html>
