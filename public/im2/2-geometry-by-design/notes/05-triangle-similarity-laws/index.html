<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
  <meta charset="utf-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
    
  <title>Triangle Similarity Laws</title>
  <meta name="description" content="AA~, SSS~, and SAS~"/>
  
  <link rel="stylesheet" href="/global-style.css"/>
  <link rel="stylesheet" href="/im2-style.css"/>
  
  <link rel="stylesheet"
    href="https://cdn.jsdelivr.net/npm/katex@0.13.13/dist/katex.min.css"
    integrity="sha384-RZU/ijkSsFbcmivfdRBQDtwuwVqK7GMOw6IMvKyeWL2K5UAlyp6WonmB8m7Jd0Hn"
    crossorigin="anonymous"/>
  <script type="module">
      import renderMathInElement from "https://cdn.jsdelivr.net/npm/katex@0.13.13/dist/contrib/auto-render.mjs"
      renderMathInElement(document.body, {
        delimiters: [
          {left: "\\(", right: "\\)", display: false},
          {left: "\\begin{equation}", right: "\\end{equation}", display: true},
          {left: "\\begin{align}", right: "\\end{align}", display: true},
          {left: "\\begin{align*}", right: "\\end{align*}", display: true},
          {left: "\\begin{alignat}", right: "\\end{alignat}", display: true},
          {left: "\\begin{gather}", right: "\\end{gather}", display: true},
          {left: "\\begin{CD}", right: "\\end{CD}", display: true},
          {left: "\\[", right: "\\]", display: true}
        ]
      })
  </script>
  
</head>

<body>

<header class="title-block">
  <h1>Triangle Similarity Laws</h1>
  <p class="subtitle">
    Lecture Notes | Geometry by Design Lesson 5
  </p>
</header>

<section>
  <h2 id="learning-outcomes">Learning outcomes</h2>
    <p>You will…</p>
    <ol>
      <li>Understand why it is worth studying triangle similarity.</li>
      <li>Know the three triangle similarity laws.</li>
    </ol>
</section>

<section>
  <h2 id="key-vocab">Key concepts</h2>
  <ul>
    <li>
      <a href="#dfn-similarity">Similarity</a>
    </li>
    <li>
      <a href="#aa-similarity">Angle-Angle similarity (AA~) law</a>
    </li>
    <li>
      <a href="#sss-similarity">Side-Side-Side similarity (SSS~) law</a>
    </li>
    <li>
      <a href="#sas-similarity">Side-Angle-Side similarity (SAS~) law</a>
    </li>
  </ul>
</section>

<main>

<section id="introduction">
  <p>
    Earlier this week, we discussed the idea of proof. Recall that we combined existing knowledge with if/then statements to draw conclusions. For example, suppose we are given the following:
  </p>
  <ol>
    <li>If you press the red button, the machine dispenses coffee.</li>
    <li>You press the red button.</li>
  </ol>
  <p>We can conclude:</p>
  <ol start="3">
    <li>The machine dispenses coffee.</li>
  </ol>
  <p>
    If/then statements, we see, can be powerful vehicles for discovering truth. But the if/then statements we need are not always served to us on a silver platter with every problem. Luckily, it’s possible to keep a few universal if/then statements in our back pocket, ready to go at any time – these are called laws. (More specifically: theorems if they’re proven, postulates if they’re assumed.)
  </p>
  <p>
    You already have one law to use in proofs: the vertical angle theorem. In this lesson, we will continue building our stash of laws by exploring the <strong>triangle similarity laws.</strong>
  </p>
</section>

<section id="concept-of-similarity">
  <h2>Refresher on similarity</h2>
  <section class="defi-nition" id="dfn-similarity">
    <h3 class="label">Definition: Similarity</h3>
    <p>
      Two figures are <dfn>similar</dfn> if & only if they have the same shape. More formally, that means:
    </p>
    <ul>
      <li>
        All corresponding angles on the figures are congruent;
      </li>
      <li>
        All corresponding distances on the figures are proportional.
      </li>
    </ul>
    <p>In geometry, the tilde \(\sim\) means “is similar to”. So we might write things like \(\bigtriangleup ABC \sim \bigtriangleup DEF\) to mean “triangle \(ABC\) is similar to triangle \(DEF\).”</p>
  </section>
  <p>
    Similarity makes sense for any collection of geometric figures. It can be defined for straight-edged, curved, and irregular figures in 2D, 3D, and even in higher dimensions. For example, here are three similar <span title="This is not a math word">amoebas</span> having a picnic:
  </p>
  <figure>
    <img src="/im2/2-geometry-by-design/notes/05-triangle-similarity-laws/similar-amoebas-having-picnic.svg"
      role="img"
      alt="Three similar amoebas having a picnic. Colored spots and other features are uniformly scaled and rotated, and lengths in any direction retain the same relative proportions."
      class="visual-aid"
    />
    <figcaption>
      <strong>Figure 1. </strong>Three similar <a href="https://en.wikipedia.org/wiki/Amoeba" target="_blank">amoebas</a> having a picnic. Colored spots and other features are located in the same relative positions and directions, and lengths in any direction retain the same relative proportions.
    </figcaption>
  </figure>
</section>
<section id="aa-similarity">
  <h2>Angle-angle similarity</h2>
    <p>
      We will be studying <em>triangle</em> similarity in particular for two reasons:
    </p>
    <ol>
      <li>Every polygon can be decomposed into triangles.</li>
      <li>Triangles have special properties that make it easy to prove them similar.</li>
    </ol>
    <p>
      There is a reason why angles and distances both show up in the definition of similarity. In general, you can’t say two objects are similar just because they have the same angles. Consider the rectangles in figure 2. Although all four pairs of corresponding angles are congruent (90°), the rectangles are not similar.
    </p>
  <figure>
    <img src="/im2/2-geometry-by-design/notes/05-triangle-similarity-laws/dissimilar-rectangles.svg"
      role="img"
      alt="Two dissimilar rectangles"
      class="visual-aid"
    />
    <figcaption><strong>Figure 2. </strong>Two dissimilar rectangles.</figcaption>
  </figure>
    <p>
      But surprisingly, congruent angles <em>are</em> enough to prove <em>triangles</em> similar. Even more surprisingly, you don’t need to look at all three angles; just two:
    </p>
    <section class="theo-rem">
      <h3 class="label">Postulate: Angle-Angle similarity (AA~)</h3>
      <p>
        Two triangles are similar if two angles of one are equal to two angles of the other.
      </p>
      <figure>
        <img src="/im2/2-geometry-by-design/notes/05-triangle-similarity-laws/aa-diagram.svg"
          role="img"
          alt="Triangles with two pairs of congruent corresponding angles and similarity symbol between them"
          class="visual-aid"
        />
        <figcaption><strong>Figure 3. </strong>Angle-Angle similarity (AA~)</figcaption>
      </figure>
    </section>
    <p>
      We will be making heavy use of all these similarity laws, so write them down!
    </p>
</section>
<section id="sss-similarity">
  <section class="theo-rem">
    <h3 class="label">Theorem: Side-Side-Side similarity (SSS~)</h3>
    <p>
      Two triangles are similar if their sides are respectively proportional.
    </p>
  </section>
  <figure>
    <img src="/im2/2-geometry-by-design/notes/05-triangle-similarity-laws/sss-diagram.svg"
      role="img"
      alt="Triangles with three pairs of proportional corresponding sides and similarity symbol between them"
      class="visual-aid"
    />
    <figcaption><strong>Figure 4. </strong>Side-Side-Side similarity (SSS~)</figcaption>
  </figure>
  <p>Note: when one geometric figure has side lengths \(r\) times as long as another, we call that number \(r\) the <dfn id="dfn-length-ratio">length ratio</dfn> or <dfn id="dfn-scale-factor">scale factor</dfn>.</p>
</section>
<section id="sas-similarity">
  <section class="theo-rem">
    <h3 class="label">Theorem: Side-Angle-Side similarity (SAS~)</h3>
    <p>
      Two triangles are similar if:
    </p>
    <ol type="i">
      <li>an angle of one equals an angle of the other;</li>
      <li>the sides <em>including</em> these angles are proportional.</li>
    </ol>
  </section>
  <figure>
    <img src="/im2/2-geometry-by-design/notes/05-triangle-similarity-laws/sas-diagram.svg"
      role="img"
      alt="Triangles with a pair of equal angles and sides including said angle proportional, with similarity symbol between them"
      class="visual-aid"
    />
    <figcaption><strong>Figure 4. </strong>Side-Angle-Side similarity (SAS~)</figcaption>
  </figure>
</section>


</main>

</body>
</html>
