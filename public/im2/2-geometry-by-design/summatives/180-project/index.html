<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
  <meta charset="utf-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
    
  <title>180 Project</title>
  <meta
    name="description"
    content="A project for Integrated Math 2 that shows whether students can
    write a proof."/>
  
  <link rel="stylesheet" href="/global-style.css"/>
  <link rel="stylesheet" href="/im2-style.css"/>
  <link rel="stylesheet" href="/exercises-style.css"/>
  
  <link rel="stylesheet"
    href="https://cdn.jsdelivr.net/npm/katex@0.13.13/dist/katex.min.css"
    integrity="sha384-RZU/ijkSsFbcmivfdRBQDtwuwVqK7GMOw6IMvKyeWL2K5UAlyp6WonmB8m7Jd0Hn"
    crossorigin="anonymous"/>
  <script type="module">
      import renderMathInElement from "https://cdn.jsdelivr.net/npm/katex@0.13.13/dist/contrib/auto-render.mjs";
      renderMathInElement(document.body, {
        delimiters: [
          {left: "\\(", right: "\\)", display: false},
          {left: "\\begin{equation}", right: "\\end{equation}", display: true},
          {left: "\\begin{align}", right: "\\end{align}", display: true},
          {left: "\\begin{align*}", right: "\\end{align*}", display: true},
          {left: "\\begin{alignat}", right: "\\end{alignat}", display: true},
          {left: "\\begin{gather}", right: "\\end{gather}", display: true},
          {left: "\\begin{CD}", right: "\\end{CD}", display: true},
          {left: "\\[", right: "\\]", display: true}
        ]
      })
  </script>
  
</head>

<body>
<aside>
  <p>
    <label>
      Name: <input type="text" name="student-name" id="student-name"/>
    </label>
  </p>
  <p>
    <label>
      Date started: <input type="date" name="date-started" id="date-started"/>
    </label>
  </p>
  <p>
    <label>
      IM2 teacher: <input type="text" name="im2-teacher" id="im2-teacher"/>
    </label>
  </p>
</aside>
<header class="title-block">
  <h1>The 180 Project</h1>
  <p class="subtitle">
    Summative Project
  </p>
</header>

<main>
<section id="introduction">
  <h2>Introduction</h2>
  <p>
    You are an ancient Greek geometry student, working alongside Euclid to uncover the foundations of geometry. After days – <em>days</em> – of studying triangles, you think you might have made a discovery: that the three angles of a triangle always sum to 180°. (This is known as the <strong>triangle sum theorem</strong>.)
  </p>
  <p>
    You tell your fellow students, but they’re skeptical. How could the angles in every triangle add up to the same thing? It’s hard to believe, they say.
  </p>
  <p>
    You must walk them through the logic using the power of proof, <strong>without</strong> assuming that the angles of a triangle add up to 180°…
  </p>
</section>
<section id="situation">
  <h2>Situation</h2>
  <p>
    All of the problems refer to <a href="#figure_180-project-diagram-concrete">the diagram below</a>: we are given \(\bigtriangleup ABC\) with…
  </p>
  <ul>
    <li>angles \(\angle A = 65°\), \(\angle B = \theta\) (unknown), and \(\angle C = 72°\);</li>
    <li>side lengths \(\overline{BC} = 26\) cm, \(\overline{CA} = 20\) cm, and \(\overline{AB} = 28\) cm.</li>
  </ul>
  <p>
    On top of this, some scratch work has already been done for you:
    <ul>
      <li>point \(M\) has been constructed exactly halfway between points \(C\) and \(A\) (i.e. \(M\) is the <strong>midpoint</strong> of \(\overline{CA}\));</li>
      <li>line segment \(\overline{LM}\) has been constructed parallel to segment \(\overline{AB}\), with point \(L\) on \(\overline{BC}\);</li>
      <li>line segment \(\overline{MN}\) has been constructed parallel to segment \(\overline{BC}\), with point \(N\) on \(\overline{AB}\);</li>
    </ul>
  </p>
  <p>
    <strong>Our ultimate goal is to prove that \(\angle A + \angle B + \angle C = 180°\).</strong> The particular side lengths and angle measures we have chosen are only to make it easier for you to understand. (In fact, if you finish early and want an extra challenge, try proving the result in general. That is, answer the same set of questions found here, but use <a href="/im2/2-geometry-by-design/summatives/180-project/180-project-diagram-abstract.png">a diagram of an arbitrary triangle instead</a>.)
  </p>
  <h3>Diagram</h3>
  <figure id="figure_180-project-diagram-concrete">
    <img src="/im2/2-geometry-by-design/summatives/180-project/180-project-diagram-concrete.png" alt="Triangle ABC with medial triangle LMN inscribed."/>
    <figcaption>
      <em>Aside: All measurements on this triangle are rounded to the nearest integer. This is to make calculation easier. In reality, sides \(\overline{AB}\) and \(\overline{BC}\) have a ton of nonzero digits after the decimal point; however, they do not affect the underlying logic of our proof, so we will ignore them.</em>
    </figcaption>
  </figure>
  <h3>Helpful vocabulary</h3>
  <ul class="word-bank" style="height: 8em;">
    <li>Line</li>
    <li>Line segment</li>
    <li>Parallel</li>
    <li>Corresponding angles (parallel lines)</li>
    <li>Supplementary angles</li>
    <li>Complementary angles</li>
    <li>Triangle sum theorem</li>
    <li>Similar</li>
    <li>Angle-Angle Similarity (AA~)</li>
    <li>Side-Side-Side Similarity (SSS~)</li>
    <li>Side-Angle-Side Similarity (SAS~)</li>
    <li>Corresponding angles (similarity)</li>
    <li>Corresponding lengths</li>
  </ul>
</section>
<section id="problems">
  <div class="exercise-count-reset"></div>
  <h2>Problems</h2>
  <section id="point-M">
    <h3>Part 1: Point <em>M</em></h3>
    <ol class="exer-cises">
      <li>
        <label for="lengths-CM-MA">
          Read through the situation carefully. In your own words, how is point \(M\) related to points \(A\) and \(C\)? Use this relationship to find the lengths of \(\overline{CM}\) and \(\overline{MA}\), and add them to the diagram.
        </label>
      </li>
    </ol>
    <textarea
      id="lengths-CM-MA"
      name="lengths-CM-MA"
      rows="3"
      cols="72"
    ></textarea>
  </section>
  <section id="triangle-MLC">
    <h3>Part 2: Triangle <em>MLC</em> similar to <em>ABC</em></h3>
    <ol class="exer-cises">
      <li>
        <label for="angle-CML">
          Find the measure of \(\angle CML\) and add it to the diagram. Explain below: what relationship with another angle tells you this is true?
        </label>
      </li>
    </ol>
    <textarea
      id="angle-CML"
      name="angle-CML"
      rows="5"
      cols="72"
    >
    </textarea>
    <ol class="exer-cises">
      <li>
        <label for="MLC-sim-ABC">
          Prove that \(\bigtriangleup MLC\) is similar to \(\bigtriangleup ABC\). (Hint: use one of the three similarity laws we learned this unit.)
        </label>
      </li>
    </ol>
    <textarea
      id="MLC-sim-ABC"
      name="MLC-sim-ABC"
      rows="10"
      cols="72"
    >
    </textarea>
    <ol class="exer-cises">
      <li>
        <label for="lengths-ML-LC">
          Use the fact that \(\bigtriangleup MLC \sim \bigtriangleup ABC\) to find the lengths of segments \(\overline{ML}\) and \(\overline{LC}\), then add them to the diagram. Explain your thinking below.
        </label>
      </li>
    </ol>
    <textarea
      id="lengths-ML-LC"
      name="lengths-ML-LC"
      rows="5"
      cols="72"
    >
  </textarea>
  </section>
  <section id="triangle-ANM">
    <h3>Part 3: Triangle <em>ANM</em> similar to <em>ABC</em></h3>
    <ol class="exer-cises">
      <li>
        <label for="angle-NMA">
          Find the measure of \(\angle NMA\) and add it to the diagram. Explain below: what relationship with another angle tells you this is true?
        </label>
      </li>
    </ol>
    <textarea
      id="angle-NMA"
      name="angle-NMA"
      rows="5"
      cols="72"
    >
    </textarea>
    <ol class="exer-cises">
      <li>
        <label for="ANM-sim-ABC">
          Prove that \(\bigtriangleup ANM\) is similar to \(\bigtriangleup ABC\). (Hint: use one of the three similarity laws we learned this unit.)
        </label>
      </li>
    </ol>
    <textarea
      id="ANM-sim-ABC"
      name="ANM-sim-ABC"
      rows="10"
      cols="72"
    >
    </textarea>
    <ol class="exer-cises">
      <li>
        <label for="lengths-AN-NM">
          Use the fact that \(\bigtriangleup ANM \sim \bigtriangleup ABC\) to find the lengths of segments \(\overline{AN}\) and \(\overline{NM}\), then add them to the diagram. Explain your thinking below.
        </label>
      </li>
    </ol>
    <textarea
      id="lengths-AN-NM"
      name="lengths-AN-NM"
      rows="5"
      cols="72"
    >
  </textarea>
  <ol class="exer-cises">
    <li>
      <label for="angle-LMN">
        Find the measure of \(\angle LMN\) and add it to the diagram. Explain below: what relationship with other angles allowed you to find this value?
      </label>
    </li>
  </ol>
  <textarea
    id="angle-LMN"
    name="angle-LMN"
    rows="5"
    cols="72"
  >
  </textarea>
  </section>
  <p>
    By now, you may have noticed that two of the three angles at \(M\) are congruent to angles of triangle \(ABC\). To do the same for \(\angle LMN\), we’ll need to show that \(\bigtriangleup LMN\) is similar to \(\bigtriangleup ABC\). This is a bit of a process, so we split it into two parts:
  </p>
  <section id="triangle-NBL">
    <h3>Part 4: Triangle <em>NBL</em> similar to <em>ABC</em></h3>

    <ol class="exer-cises">
      <li>
        <label for="lengths-BL-NB">
          Find the lengths of segments \(\overline{BL}\) and \(\overline{NB}\), then add them to the diagram. Explain your thinking below.
        </label>
      </li>
    </ol>
    <textarea
      id="lengths-BL-NB"
      name="lengths-BL-NB"
      rows="3"
      cols="72"
    ></textarea>
    </ol>
    <ol class="exer-cises">
      <li>
        <label for="NBL-sim-ABC">
          Prove that \(\bigtriangleup NBL\) is similar to \(\bigtriangleup ABC\). (Hint: take another look at the diagram, and compare triangles \(NBL\) and \(ABC\). What angles do these triangles share, if any? What side lengths are proportional, if any? What similarity law does this suggest?)
        </label>
      </li>
    </ol>
    <textarea
      id="NBL-sim-ABC"
      name="NBL-sim-ABC"
      rows="10"
      cols="72"
    ></textarea>
    <ol class="exer-cises">
      <li>
        <label for="length-NL">
          Use the fact that \(\bigtriangleup NBL \sim \bigtriangleup ABC\) to find the length of segment \(\overline{NL}\), then add it to the diagram. Explain your thinking below.
        </label>
      </li>
    </ol>
    <textarea
      id="length-NL"
      name="length-NL"
      rows="3"
      cols="72"
    >
  </textarea>
  </section>
  <section id="triangle-LMN">
    <h3>Part 5: Triangle <em>LMN</em> similar to <em>ABC</em></h3>
    <ol class="exer-cises">
      <li>
        <label for="LMN-sim-ABC">
          Prove that \(\bigtriangleup LMN\) is similar to \(\bigtriangleup ABC\). (Hint: use one of the three similarity laws we learned this unit.)
        </label>
      </li>
    </ol>
    <textarea
      id="LMN-sim-ABC"
      name="LMN-sim-ABC"
      rows="10"
      cols="72"
    ></textarea>
    <ol class="exer-cises">
      <li>
        <label for="final-conclusion">
          Which angle of \(\bigtriangleup ABC\) corresponds to \(\angle LMN\)? Use this fact to find the measure of \(\angle B\).
        </label>
      </li>
    </ol>
    <textarea
      id="final-conclusion"
      name="final-conclusion"
      rows="3"
      cols="72"
    ></textarea>
  </section>
  
</section>

<section id="rubric">
  <h2>Rubric</h2>
    <table class="printable-table">
      <thead>
        <tr>
          <th>Criterion</th>
          <th>8: Strongly demonstrated</th>
          <th>7: Demonstrated</th>
          <th>6: Partly demonstrated</th>
          <th>5: Weakly demonstrated</th>
          <th>4: Not demonstrated</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>
            <strong>Proofs</strong>
          </td>
          <td>
            Each proof clearly contains a chain of reasoning from assumptions to conclusion. All steps in your proofs are correct. Everything you write either furthers the argument or clarifies the situation for your reader. You deeply understand what a proof is and do not confuse it with informal argument.
          </td>
          <td>
            Your proofs contain chains of reasoning from assumptions to conclusion. Most steps in your proofs are correct. Most of what you write either furthers the argument or clarifies the situation for your reader. You understand what a proof is, and only small parts of your proofs are too vague or informal.
          </td>
          <td>
            Your proofs sometimes contain chains of reasoning from assumptions to conclusion. Most steps in your proofs are correct, but there is some confusion between a proof and informal argument. However, there are enough elements that are proof-like to get the point across.
          </td>
          <td>
            You only present informal arguments, not proofs.
          </td>
          <td>
            Nothing to assess.
          </td>
        </tr>
        <tr>
          <td>
            <strong>Mathematical reasoning</strong></td>
          <td>
            You spell your thinking out so clearly that any of your classmates could understand it. You describe every step of your process with words, data, pictures, or symbols. Your solution processes contain almost no errors.
          </td>
          <td>
            You spell your thinking out clearly enough that most of your classmates could understand it. You describe most steps with words, data, pictures, or symbols; a few steps are unclear. Your solution processes contain few or minor errors.
          </td>
          <td>
            Some classmates could understand what you did, with effort. You describe some steps with words, data, pictures, and symbols; other steps are unclear. You make significant errors in solving some problems.
          </td>
          <td>
            You either wrote just the answer or complete gobbledygook.
          </td>
          <td>
            Nothing to assess.
          </td>
        </tr>
        <tr>
          <td>
            <strong>Understanding of geometry</strong>
          </td>
          <td>
            Your explanations show a deep understanding of angles & lines, similarity, and transformations. You consistently make use of the information (e.g. angle & length measurements, givens) and principles (e.g. similarity laws, special types of angles, definitions) most relevant to the problem.
          </td>
          <td>
            Your explanations show a working understanding of angles & lines, similarity, and transformations. You usually make use of the information (e.g. angle & length measurements, definitions, givens) and principles (e.g. similarity laws, special types of angles, definitions) most relevant to the problem.
          </td>
          <td>
            Your explanations show a partial understanding of angles & lines, similarity, and transformations. You sometimes make use of the information (e.g. angle & length measurements, definitions, givens) and principles (e.g. similarity laws, special types of angles, definitions) most relevant to the problem.
          </td>
          <td>
            Your explanations do not show understanding of angles & lines, similarity, and transformations. You do not make use of the information (e.g. angle & length measurements, definitions, givens) and principles (e.g. similarity laws, special types of angles, definitions) most relevant to the problem.
          </td>
          <td>Nothing to assess.</td>
        </tr>
      </tbody>
    </table>
</section>
</main>

</body>
</html>
