### A Pluto.jl notebook ###
# v0.18.0

using Markdown
using InteractiveUtils

# ╔═╡ b6ca4a64-a546-11ec-1bad-139b7b294d3d
begin
  using DataFrames
  using Distributions
  using LinearAlgebra
  using Statistics
  using Test
  using Random
end

# ╔═╡ d5215c6d-15b3-44b2-84c7-a54148fef7fd
md"""
# The Random Village

by Noah Scott Goldman
"""

# ╔═╡ a1b2a449-271c-4437-a4cc-08243b7506bb
md"""
![Village](https://images.unsplash.com/photo-1536308037887-165852797016?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1652&q=80)

*This assignment is a Pluto notebook written in the [Julia programming language](https://julialang.org). If you would like to peek at the code that makes this magic happen, click the 👁️ next to the cell to see it. Don’t modify code that is hidden, though; you might break the assignment!*
"""

# ╔═╡ 9959fdd6-9135-4154-9085-3d053c720369
md"""
## Objectives

1. See what expected value means using a computer simulation.

2. Predict the average outcome of a simulation using probability and expected value.

3. (Optional) Understand that the sample mean approaches the expected value as the sample size increases.
"""

# ╔═╡ d4ea373d-d4e3-4a42-9eec-4b158a1f5d07
md"""
## Part 1: Name your village

**First, name your village.** Make sure you enclose the name in quotes (e.g. `"Expectovalisi"`) then click ▶️ or press Shift+Enter.
"""

# ╔═╡ 94b1d0e9-e33a-411f-b2e2-e52ac22b5815
village = "Expectovalisi";

# ╔═╡ 2c353035-7994-4b4f-bd67-23ec2077f598
md"""
Citizens of America are called Americans. Citizens of Georgia are called Georgians. **What will citizens of your village be called?** (For this and all other cells, click ▶️ or press Shift+Enter to run your code.)
"""

# ╔═╡ eea6c43b-61fe-4b4a-a67a-d2dc98a7ec61
villager_singular = "Expectovalian";

# ╔═╡ a2803c70-8de4-4952-9577-68bead54f6ae
villager_plural = "Expectovalians";

# ╔═╡ 510f7e94-0089-4ea1-ad2b-3e4fa58c0a1f
md"""
### Introduction

In a magical land, you are the mayor of a village called *$(village)*. It is your job to make sure the village has food. So, every day, you send out three groups of $(villager_plural) to forage: hunters to hunt meat 🥩, gatherers to gather fruits & vegetables 🥬, and wizards to make pizza 🧙.

You suspect that one group is bringing home more food than the others, and you will test this theory using expected value and probability.
"""

# ╔═╡ d778079f-2d2a-4d63-99ee-233c691f84ef
md"""
## Part 2: Hunters 🥩

When the $(villager_singular) hunters go hunting, they can carry back at most one animal. The most common animals in the area are rabbits; turkeys are somewhat rarer. However, sometimes a hunter has bad luck and can’t bring back anything.

Here are the foods hunters can bring back. (Note: in Julia, `2//7` just means 2/7, but not rounded to a decimal.)
"""

# ╔═╡ 0c52ea76-e202-4f32-b5ae-a6bc0e268bab
hunter_foods = DataFrame(
	icon = ['😑', '🐇', '🦃'],
	name = ["No food", "Rabbit", "Turkey"],
	calories = [0.0, 188.0, 235.0],
	probability = [14//47, 20//47, 13//47]
)

# ╔═╡ e14a833e-eca7-42fa-8091-1279b4fc03b8
md"""
Let’s run a simulation using the values from the table. First, we need to set up something called a `UnivariateDistribution` with the probabilities from the table.

**Type the following code into the cell below, exactly as you see it.** Press `Tab` to indent. (Remember to run the code!)
```julia
HunterDist = DiscreteNonParametric(
	hunter_foods[!, :calories],
	hunter_foods[!, :probability]
)
```
"""

# ╔═╡ b626a9c3-a1f3-440f-8af2-fc23b703d993


# ╔═╡ 15bafb5c-0962-4d0f-a0c2-9721a797883d
md"""
Essentially, we have told Julia to create a three-sided “die”. “Rolling” this die simulates sending a hunter out for food. The outcome of this “hunt”, $\text{Calories}$, takes on the calorie values listed above with the probabilities listed above.
"""

# ╔═╡ da1aced9-9c44-4bb0-a9c9-252086ea401b
md"""
Now, let’s simulate five of our hunters going out for food.

**Type the following code into the cell below, exactly as you see it.** (Remember to run the code!)
```julia
hunters5 = rand(HunterDist, 5)
```
"""

# ╔═╡ b423b186-ebc6-4665-a434-f4f5145d94e4


# ╔═╡ c2dc2717-7b16-4eb7-9df1-5b88c4f5e73f
md"""
The code above tells Julia, “Generate 5 random values from `HunterDist`.” You should have a list of five random calorie values, each of which is equal to 0, 188, or 235. If you run the code again, Julia will generate five new values (effectively “sending the hunters out again”).

Let’s find the sample mean (a.k.a. the average) of these values.

**Type the following code into the cell below, exactly as you see it.** (Remember to run the code!)

```julia
hunter_mean5 = mean(hunters5)
```
"""

# ╔═╡ f9646774-72e8-4db8-b271-35e6bbf41977


# ╔═╡ f49ec026-62ff-4eb9-89a9-ca4a3a0d44f9
md"""
### Problems

  1. **Simulate 256 hunters going out for food in the cell below.** Name the resulting list `hunters256`.
"""

# ╔═╡ 21968242-efda-4902-8e93-d8af6f31ec25
hunters256 = [] # Replace `[]` with your code

# ╔═╡ 62f6c294-3684-4eeb-bfbd-06c10dc7fdc6
md"""
  2. **Find the sample mean of `hunters256`.** Name the result `hunter_mean256`.
"""

# ╔═╡ 73682d5d-e2de-4fcf-b416-80744a6f5481
hunter_mean256 = 0 # Replace `0` with your code

# ╔═╡ 5934c827-508b-4249-9f4a-20d74a5a8de0
html"""
<label>
	Explain what this value means below: <br />
	<textarea cols='60'>This value represents…</textarea>
</label>
"""

# ╔═╡ 4cbfb6fb-9198-4522-b5b0-af3567458a79
md"""
  3. **Using the `hunter_foods` table (scroll up), find the expected amount of calories a hunter brings back per day.** Set `hunter_expected_cal` equal to your answer below. A message should tell you if you are correct.
"""

# ╔═╡ 31bab126-68cf-4cc8-a848-8477a70d6044
hunter_expected_cal = 0 # Replace `0` with your code

# ╔═╡ 7ef684b1-9ae5-4cc6-b507-053c8e38492f
html"""
<label>
	Explain what this value means below: <br />
	<textarea cols='60' rows='3'></textarea>
</label>
"""

# ╔═╡ ad9cc2f7-142e-49ee-9921-b44f86d234db
md"""
  4. Below are the *logarithmic differences* between your simulated mean and the expected value, in both the 5- and 256-hunter simulations. The closer this number is to 0, the closer the two values. A log-difference between −0.05 and 0.05 is very, very close (roughly between −5% and 5%). **Are your sample means close to the expected value? Why do you think this is or isn’t so?**
"""

# ╔═╡ ce7d95b3-84df-4980-aaee-0e16acb72400
log(hunter_mean5/hunter_expected_cal)

# ╔═╡ dd3c6f40-cf8b-43f5-99c2-2dd2877e4844
ld_h256 = log(hunter_mean256/hunter_expected_cal)

# ╔═╡ 15559aed-2307-4eee-9269-77599b720382
html"""
<label>
	Answer here: <br />
	<textarea cols='60' rows='10'></textarea>
</label>"""

# ╔═╡ 419b5bc2-a83b-443e-a56d-d686e63c5c54
md"""
  5. (Optional) Create your own cells and experiment with the number of hunters (a.k.a. the *sample size*). Play around with 512, 1024, or even more hunters, and calculate the mean & log difference. **What happens to the log difference as the sample size increases?**
"""

# ╔═╡ ca4fc529-66dc-41dc-aada-f882b310ee9c


# ╔═╡ 58ddc742-7b49-4ab6-81ac-74065380af9d
md"""
## Part 3: Wizards 🧙

The $(villager_singular) wizards have spent years developing a spell to create pizza. Pizza is the highest-calorie food available to $(village). However, each wizard can only cast the spell once per day and has a chance of failing entirely. Even if the spell works… most of the time, the pizza ends up with pineapple on it!

Here are the foods wizards can produce:
"""

# ╔═╡ f8a63f8b-dc33-40fe-98ea-23445c2d9790
wizard_foods = DataFrame(
	icon = ['😑', '🍍', '🍕'],
	name = ["No food", "Pineapple pizza", "Pepperoni pizza"],
	calories = [0.0, 282.0, 329.0],
	probability = [28//47, 12//47, 7//47]
)

# ╔═╡ 1a3bc865-2bb4-419c-b007-1eb3e9827b20
md"""
We’ll simulate the wizards the same way we simulate the hunters.

**Type the following code into the cell below, exactly as you see it.** Press `Tab` to indent. (Remember to run the code!)
```julia
WizardDist = DiscreteNonParametric(
	wizard_foods[!, :calories],
	wizard_foods[!, :probability]
)
```
"""

# ╔═╡ cf363f68-75bf-43e2-9e76-ae374256fca1


# ╔═╡ 75df6409-5d9e-4c10-bf75-8c102d61edec
md"""
### Problems

  1. **Simulate 256 wizards trying to produce food in the cell below.** Name the resulting list `wizards256`.
"""

# ╔═╡ fedaec59-23ec-4e44-a468-3b85ecf06835
wizards256 = [] # Replace `[]` with your code

# ╔═╡ 499b6f25-3ed9-4135-931c-d4f15e954aec
md"""
  2. **Find the sample mean of `wizards256`.** Name the result `wizard_mean256`.
"""

# ╔═╡ 7dfeb9a5-0276-4218-81b6-4a59fa4f7ebd
wizard_mean256 = 0 # Replace `0` with your code

# ╔═╡ 8202d4c3-f18c-4d48-b689-080701b76589
html"""
<label>
	Explain what this value means below: <br />
	<textarea cols='60'></textarea>
</label>
"""

# ╔═╡ 19abed41-4860-452e-b1ea-a2e30d29b49f
md"""
  3. **Using the `wizard_foods` table (scroll up), find the expected amount of calories a hunter brings back per day.** Set `wizard_expected_cal` equal to your answer below. A message should tell you if you are correct.
"""

# ╔═╡ 6ee8853f-cbff-4207-af59-053e9cd7daf5
wizard_expected_cal = 0 # Replace `0` with your code

# ╔═╡ 476d4097-77ee-4bfb-80a0-6d5c0f5ffa2c
html"""
<label>
	Explain what this value means below: <br />
	<textarea cols='60' rows='3'></textarea>
</label>
"""

# ╔═╡ 3231be0f-3a66-4214-8d2f-c362308f3650
md"""
  4. Below is the *logarithmic difference* between your sample mean and the expected value. The closer this number is to 0, the closer the two values. A log-difference between −0.05 and 0.05 means the two values are very, very close (roughly between −5% and 5%). **Is your sample mean close to the expected value? Why do you think this is or isn’t so?**
"""

# ╔═╡ bdb6da88-d156-4197-9a45-e6c5b0543d10
ld_w256 = log(wizard_mean256/wizard_expected_cal)

# ╔═╡ bdd096fd-5822-41b8-ae6f-9c25c127fae3
html"""
<label>
	Answer here: <br />
	<textarea cols='60' rows='4'></textarea>
</label>"""

# ╔═╡ 98d3b087-3402-43c1-bcbb-766b70ca5dde
md"""
## Part 4: Gatherers 🥬

Unlike wizards and hunters, the $(villager_singular) gatherers never come back empty-handed. That said, they bring back plant-based foods low in calories.

Here are the foods gatherers can find:
"""

# ╔═╡ 820a4a9f-d6da-4cb9-b4cb-92e834ba0069
gatherer_foods = DataFrame(
	icon = ['🧅', '🍑', '🍉'],
	name = ["Onion", "Mega peach", "Watermelon"],
	calories = [78 + 1/3, 164.5, 211.5],
	probability = [9 // 47,  14 // 47, 24 // 47]
)

# ╔═╡ de562746-0a7e-4f22-b30a-6f145d429583
md"""
We’ll simulate the gatherers the same way we simulate the other two groups.

**Type the following code into the cell below, exactly as you see it.** Press `Tab` to indent. (Remember to run the code!)
```julia
GathererDist = DiscreteNonParametric(
	gatherer_foods[!, :calories],
	gatherer_foods[!, :probability]
)
```
"""

# ╔═╡ 9d48819f-6278-483f-b34a-6a5410bae48c


# ╔═╡ 0fccc3cb-5f8a-4d0d-9e8c-10d97361154c
md"""
### Problems

  1. **Simulate 256 gatherers gathering food in the cell below.** Name the resulting list `gatherers256`.
"""

# ╔═╡ 60995ad2-5da2-4582-b1de-498eb3d31961
gatherers256 = [] # Replace `[]` with your code

# ╔═╡ 24ef595f-e4f3-4ab6-aa16-0ccb995ccf68
md"""
  2. **Find the sample mean of `gatherers256`.** Name the result `gatherer_mean256`.
"""

# ╔═╡ f9416c50-aef9-448d-9b24-53235997bbcf
gatherer_mean256 = 0 # Replace `0` with your code

# ╔═╡ 879c636d-cb6b-4312-84ac-ddc6cb228514
md"""
  3. **Using the `gatherer_foods` table (scroll up), find the expected amount of calories a hunter brings back per day.** Set `gatherer_expected_cal` equal to your answer below. A message should tell you if you are correct.
"""

# ╔═╡ e80898b3-e0c5-4363-836a-d157719dfd0a
gatherer_expected_cal = 0 # Replace `0` with your code

# ╔═╡ d2453234-f8e3-4f46-9350-92a0287e4df3
md"""
  4. Below is the *logarithmic difference* between your sample mean and the expected value. The closer this number is to 0, the closer the two values. A log-difference between −0.05 and 0.05 means the two values are very, very close (roughly between −5% and 5%). **Is your sample mean close to the expected value? Why do you think this is or isn’t so?**
"""

# ╔═╡ 5c6e701c-af10-4f51-aa63-0e291d9dbdd4
ld_g256 = log(gatherer_mean256/gatherer_expected_cal)

# ╔═╡ 9c9224f3-01cd-49f7-98bf-252b46ea02ed
html"""
<label>
	Answer here: <br />
	<textarea cols='60' rows='4'></textarea>
</label>"""

# ╔═╡ 25416763-56e5-4db2-891a-d3c4bd352a30
md"""
## Part 5: Connecting experiments to theory
"""

# ╔═╡ 878f253a-e36e-4c33-9f73-305bbfcb47e8
md"""
Below is a summary of the information you’ve gathered so far:
"""

# ╔═╡ 6580bd8d-5486-45f7-88cf-81d180253404
DataFrame(
	icon = ['🥩', '🧙', '🥬'],
	group = ["Hunters", "Wizards", "Gatherers"],
	expected_value = [hunter_expected_cal, wizard_expected_cal, gatherer_expected_cal],
	sample_mean = [hunter_mean256, wizard_mean256, gatherer_mean256],
	log_diff = [ld_h256, ld_w256, ld_g256]
)

# ╔═╡ 038fb6f0-ac14-4a66-ab03-6097ed3f79e4
md"""
  1. Suppose you had 1,000 villagers to assign roles (hunter, wizard, or gatherer). To maximize their calorie output, how many of each role would you assign (according to the work you’ve done so far)?
"""

# ╔═╡ bc9962c4-1a02-42f2-83da-ebaf9800ebcb
html"""
<label>
	Answer here: <br />
	<textarea cols='60' rows='4'></textarea>
</label>"""

# ╔═╡ 533a6ce8-4e91-41d1-986c-91df884e29cb
md"""
2. Make a conjecture about the relationship between the expected value and sample mean as the sample size approaches ∞.
"""

# ╔═╡ c827f930-9042-47f4-89ff-849f69550084
html"""
<details>
	<summary><strong>Hint</strong></summary>
	“As the sample size approaches ∞…”
</details>
"""

# ╔═╡ a44013a6-fce8-49e7-9278-884a4635f13a
html"""
<label>
	Answer here: <br />
	<textarea cols='60' rows='6'></textarea>
</label>"""

# ╔═╡ 097bfc70-b44c-4e6f-a456-a80d12c5c3a2
md"""
3. Look up the Law of Large Numbers. What is the Law of Large Numbers? How is it related to what we have done here?
"""

# ╔═╡ 14ca1f9f-243a-46b2-9c06-1ebf550ed930
html"""
<label>
	Answer here: <br />
	<textarea cols='60' rows='5'></textarea>
</label>"""

# ╔═╡ 219001df-0c33-4d16-9ab0-36545f59ffa1
function check_answer(
	ans_cond::Bool, # answer condition
	accept_msg::Markdown.MD = md"🥳 **Woo-hoo!** You’re ready to move on to the next question.",
	reject_msg::Markdown.MD = md"🪳 **Hmm, not there yet.** Check your code for bugs and see if you can fix it."
)
	if ans_cond == true
		accept_msg
	else
		reject_msg
	end
end;

# ╔═╡ d35d3327-16c2-44c1-b09d-15d64e9a9430
check_answer(
	abs(hunter_foods[!, :calories] ⋅ hunter_foods[!, :probability] - hunter_expected_cal) < 0.01,
	md"🥳 **Woo-hoo!** That is indeed the expected value.",
	md"🪳 **Hmm, not there yet.** Double-check your calculations and try again."
)

# ╔═╡ d63b0870-c1f1-47c0-9946-d97fc11127c8
check_answer(
	abs(wizard_foods[!, :calories] ⋅ wizard_foods[!, :probability] - wizard_expected_cal) < 0.01,
	md"🥳 **Woo-hoo!** That is indeed the expected value.",
	md"🪳 **Hmm, not there yet.** Double-check your calculations and try again."
)

# ╔═╡ 40bbd304-b372-4bcc-ac02-b32fd3a6f76a
check_answer(
	abs(gatherer_foods[!, :calories] ⋅ gatherer_foods[!, :probability] - gatherer_expected_cal) < 0.01,
	md"🥳 **Woo-hoo!** That is indeed the expected value.",
	md"🪳 **Hmm, not there yet.** Double-check your calculations and try again."
)

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
DataFrames = "a93c6f00-e57d-5684-b7b6-d8193f3e46c0"
Distributions = "31c24e10-a181-5473-b8eb-7969acd0382f"
LinearAlgebra = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"
Random = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"
Statistics = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"
Test = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[compat]
DataFrames = "~1.3.2"
Distributions = "~0.25.51"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.2"
manifest_format = "2.0"

[[deps.ArgTools]]
uuid = "0dad84c5-d112-42e6-8d28-ef12dabb789f"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.Calculus]]
deps = ["LinearAlgebra"]
git-tree-sha1 = "f641eb0a4f00c343bbc32346e1217b86f3ce9dad"
uuid = "49dc2e85-a5d0-5ad3-a950-438e2897f1b9"
version = "0.5.1"

[[deps.ChainRulesCore]]
deps = ["Compat", "LinearAlgebra", "SparseArrays"]
git-tree-sha1 = "c9a6160317d1abe9c44b3beb367fd448117679ca"
uuid = "d360d2e6-b24c-11e9-a2a3-2a2ae2dbcce4"
version = "1.13.0"

[[deps.ChangesOfVariables]]
deps = ["ChainRulesCore", "LinearAlgebra", "Test"]
git-tree-sha1 = "bf98fa45a0a4cee295de98d4c1462be26345b9a1"
uuid = "9e997f8a-9a97-42d5-a9f1-ce6bfc15e2c0"
version = "0.1.2"

[[deps.Compat]]
deps = ["Base64", "Dates", "DelimitedFiles", "Distributed", "InteractiveUtils", "LibGit2", "Libdl", "LinearAlgebra", "Markdown", "Mmap", "Pkg", "Printf", "REPL", "Random", "SHA", "Serialization", "SharedArrays", "Sockets", "SparseArrays", "Statistics", "Test", "UUIDs", "Unicode"]
git-tree-sha1 = "96b0bc6c52df76506efc8a441c6cf1adcb1babc4"
uuid = "34da2185-b29b-5c13-b0c7-acf172513d20"
version = "3.42.0"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"

[[deps.Crayons]]
git-tree-sha1 = "249fe38abf76d48563e2f4556bebd215aa317e15"
uuid = "a8cc5b0e-0ffa-5ad4-8c14-923d3ee1735f"
version = "4.1.1"

[[deps.DataAPI]]
git-tree-sha1 = "cc70b17275652eb47bc9e5f81635981f13cea5c8"
uuid = "9a962f9c-6df0-11e9-0e5d-c546b8b5ee8a"
version = "1.9.0"

[[deps.DataFrames]]
deps = ["Compat", "DataAPI", "Future", "InvertedIndices", "IteratorInterfaceExtensions", "LinearAlgebra", "Markdown", "Missings", "PooledArrays", "PrettyTables", "Printf", "REPL", "Reexport", "SortingAlgorithms", "Statistics", "TableTraits", "Tables", "Unicode"]
git-tree-sha1 = "ae02104e835f219b8930c7664b8012c93475c340"
uuid = "a93c6f00-e57d-5684-b7b6-d8193f3e46c0"
version = "1.3.2"

[[deps.DataStructures]]
deps = ["Compat", "InteractiveUtils", "OrderedCollections"]
git-tree-sha1 = "3daef5523dd2e769dad2365274f760ff5f282c7d"
uuid = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
version = "0.18.11"

[[deps.DataValueInterfaces]]
git-tree-sha1 = "bfc1187b79289637fa0ef6d4436ebdfe6905cbd6"
uuid = "e2d170a0-9d28-54be-80f0-106bbe20a464"
version = "1.0.0"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.DelimitedFiles]]
deps = ["Mmap"]
uuid = "8bb1440f-4735-579b-a4ab-409b98df4dab"

[[deps.DensityInterface]]
deps = ["InverseFunctions", "Test"]
git-tree-sha1 = "80c3e8639e3353e5d2912fb3a1916b8455e2494b"
uuid = "b429d917-457f-4dbc-8f4c-0cc954292b1d"
version = "0.4.0"

[[deps.Distributed]]
deps = ["Random", "Serialization", "Sockets"]
uuid = "8ba89e20-285c-5b6f-9357-94700520ee1b"

[[deps.Distributions]]
deps = ["ChainRulesCore", "DensityInterface", "FillArrays", "LinearAlgebra", "PDMats", "Printf", "QuadGK", "Random", "SparseArrays", "SpecialFunctions", "Statistics", "StatsBase", "StatsFuns", "Test"]
git-tree-sha1 = "43ea1b7ab7936920a1170d6a35f05a1f9f495216"
uuid = "31c24e10-a181-5473-b8eb-7969acd0382f"
version = "0.25.51"

[[deps.DocStringExtensions]]
deps = ["LibGit2"]
git-tree-sha1 = "b19534d1895d702889b219c382a6e18010797f0b"
uuid = "ffbed154-4ef7-542d-bbb7-c09d3a79fcae"
version = "0.8.6"

[[deps.Downloads]]
deps = ["ArgTools", "LibCURL", "NetworkOptions"]
uuid = "f43a241f-c20a-4ad4-852c-f6b1247861c6"

[[deps.DualNumbers]]
deps = ["Calculus", "NaNMath", "SpecialFunctions"]
git-tree-sha1 = "90b158083179a6ccbce2c7eb1446d5bf9d7ae571"
uuid = "fa6b7ba4-c1ee-5f82-b5fc-ecf0adba8f74"
version = "0.6.7"

[[deps.FillArrays]]
deps = ["LinearAlgebra", "Random", "SparseArrays", "Statistics"]
git-tree-sha1 = "0dbc5b9683245f905993b51d2814202d75b34f1a"
uuid = "1a297f60-69ca-5386-bcde-b61e274b549b"
version = "0.13.1"

[[deps.Formatting]]
deps = ["Printf"]
git-tree-sha1 = "8339d61043228fdd3eb658d86c926cb282ae72a8"
uuid = "59287772-0a20-5a39-b81b-1366585eb4c0"
version = "0.4.2"

[[deps.Future]]
deps = ["Random"]
uuid = "9fa8497b-333b-5362-9e8d-4d0656e87820"

[[deps.HypergeometricFunctions]]
deps = ["DualNumbers", "LinearAlgebra", "SpecialFunctions", "Test"]
git-tree-sha1 = "65e4589030ef3c44d3b90bdc5aac462b4bb05567"
uuid = "34004b35-14d8-5ef3-9330-4cdb6864b03a"
version = "0.3.8"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.InverseFunctions]]
deps = ["Test"]
git-tree-sha1 = "91b5dcf362c5add98049e6c29ee756910b03051d"
uuid = "3587e190-3f89-42d0-90ee-14403ec27112"
version = "0.1.3"

[[deps.InvertedIndices]]
git-tree-sha1 = "bee5f1ef5bf65df56bdd2e40447590b272a5471f"
uuid = "41ab1584-1d38-5bbf-9106-f11c6c58b48f"
version = "1.1.0"

[[deps.IrrationalConstants]]
git-tree-sha1 = "7fd44fd4ff43fc60815f8e764c0f352b83c49151"
uuid = "92d709cd-6900-40b7-9082-c6be49f344b6"
version = "0.1.1"

[[deps.IteratorInterfaceExtensions]]
git-tree-sha1 = "a3f24677c21f5bbe9d2a714f95dcd58337fb2856"
uuid = "82899510-4779-5014-852e-03e436cf321d"
version = "1.0.0"

[[deps.JLLWrappers]]
deps = ["Preferences"]
git-tree-sha1 = "abc9885a7ca2052a736a600f7fa66209f96506e1"
uuid = "692b3bcd-3c85-4b1f-b108-f13ce0eb3210"
version = "1.4.1"

[[deps.LibCURL]]
deps = ["LibCURL_jll", "MozillaCACerts_jll"]
uuid = "b27032c2-a3e7-50c8-80cd-2d36dbcbfd21"

[[deps.LibCURL_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll", "Zlib_jll", "nghttp2_jll"]
uuid = "deac9b47-8bc7-5906-a0fe-35ac56dc84c0"

[[deps.LibGit2]]
deps = ["Base64", "NetworkOptions", "Printf", "SHA"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[deps.LibSSH2_jll]]
deps = ["Artifacts", "Libdl", "MbedTLS_jll"]
uuid = "29816b5a-b9ab-546f-933c-edad1886dfa8"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.LogExpFunctions]]
deps = ["ChainRulesCore", "ChangesOfVariables", "DocStringExtensions", "InverseFunctions", "IrrationalConstants", "LinearAlgebra"]
git-tree-sha1 = "58f25e56b706f95125dcb796f39e1fb01d913a71"
uuid = "2ab3a3ac-af41-5b50-aa03-7779005ae688"
version = "0.3.10"

[[deps.Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"

[[deps.Missings]]
deps = ["DataAPI"]
git-tree-sha1 = "bf210ce90b6c9eed32d25dbcae1ebc565df2687f"
uuid = "e1d29d7a-bbdc-5cf2-9ac0-f12de2c33e28"
version = "1.0.2"

[[deps.Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[deps.MozillaCACerts_jll]]
uuid = "14a3606d-f60d-562e-9121-12d972cd8159"

[[deps.NaNMath]]
git-tree-sha1 = "737a5957f387b17e74d4ad2f440eb330b39a62c5"
uuid = "77ba4419-2d1f-58cd-9bb1-8ffee604a2e3"
version = "1.0.0"

[[deps.NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"

[[deps.OpenLibm_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "05823500-19ac-5b8b-9628-191a04bc5112"

[[deps.OpenSpecFun_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "13652491f6856acfd2db29360e1bbcd4565d04f1"
uuid = "efe28fd5-8261-553b-a9e1-b2916fc3738e"
version = "0.5.5+0"

[[deps.OrderedCollections]]
git-tree-sha1 = "85f8e6578bf1f9ee0d11e7bb1b1456435479d47c"
uuid = "bac558e1-5e72-5ebc-8fee-abe8a469f55d"
version = "1.4.1"

[[deps.PDMats]]
deps = ["LinearAlgebra", "SparseArrays", "SuiteSparse"]
git-tree-sha1 = "e8185b83b9fc56eb6456200e873ce598ebc7f262"
uuid = "90014a1f-27ba-587c-ab20-58faa44d9150"
version = "0.11.7"

[[deps.Pkg]]
deps = ["Artifacts", "Dates", "Downloads", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "Serialization", "TOML", "Tar", "UUIDs", "p7zip_jll"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"

[[deps.PooledArrays]]
deps = ["DataAPI", "Future"]
git-tree-sha1 = "db3a23166af8aebf4db5ef87ac5b00d36eb771e2"
uuid = "2dfb63ee-cc39-5dd5-95bd-886bf059d720"
version = "1.4.0"

[[deps.Preferences]]
deps = ["TOML"]
git-tree-sha1 = "d3538e7f8a790dc8903519090857ef8e1283eecd"
uuid = "21216c6a-2e73-6563-6e65-726566657250"
version = "1.2.5"

[[deps.PrettyTables]]
deps = ["Crayons", "Formatting", "Markdown", "Reexport", "Tables"]
git-tree-sha1 = "dfb54c4e414caa595a1f2ed759b160f5a3ddcba5"
uuid = "08abe8d2-0d0c-5749-adfa-8a2ac140af0d"
version = "1.3.1"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.QuadGK]]
deps = ["DataStructures", "LinearAlgebra"]
git-tree-sha1 = "78aadffb3efd2155af139781b8a8df1ef279ea39"
uuid = "1fd47b50-473d-5c70-9696-f719f8f3bcdc"
version = "2.4.2"

[[deps.REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets", "Unicode"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.Reexport]]
git-tree-sha1 = "45e428421666073eab6f2da5c9d310d99bb12f9b"
uuid = "189a3867-3050-52da-a836-e630ba90ab69"
version = "1.2.2"

[[deps.Rmath]]
deps = ["Random", "Rmath_jll"]
git-tree-sha1 = "bf3188feca147ce108c76ad82c2792c57abe7b1f"
uuid = "79098fc4-a85e-5d69-aa6a-4863f24498fa"
version = "0.7.0"

[[deps.Rmath_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "68db32dff12bb6127bac73c209881191bf0efbb7"
uuid = "f50d1b31-88e8-58de-be2c-1cc44531875f"
version = "0.3.0+0"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.SharedArrays]]
deps = ["Distributed", "Mmap", "Random", "Serialization"]
uuid = "1a1011a3-84de-559e-8e89-a11a2f7dc383"

[[deps.Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[deps.SortingAlgorithms]]
deps = ["DataStructures"]
git-tree-sha1 = "b3363d7460f7d098ca0912c69b082f75625d7508"
uuid = "a2af1166-a08f-5f64-846c-94a0d3cef48c"
version = "1.0.1"

[[deps.SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[deps.SpecialFunctions]]
deps = ["ChainRulesCore", "IrrationalConstants", "LogExpFunctions", "OpenLibm_jll", "OpenSpecFun_jll"]
git-tree-sha1 = "5ba658aeecaaf96923dce0da9e703bd1fe7666f9"
uuid = "276daf66-3868-5448-9aa4-cd146d93841b"
version = "2.1.4"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[deps.StatsAPI]]
deps = ["LinearAlgebra"]
git-tree-sha1 = "c3d8ba7f3fa0625b062b82853a7d5229cb728b6b"
uuid = "82ae8749-77ed-4fe6-ae5f-f523153014b0"
version = "1.2.1"

[[deps.StatsBase]]
deps = ["DataAPI", "DataStructures", "LinearAlgebra", "LogExpFunctions", "Missings", "Printf", "Random", "SortingAlgorithms", "SparseArrays", "Statistics", "StatsAPI"]
git-tree-sha1 = "8977b17906b0a1cc74ab2e3a05faa16cf08a8291"
uuid = "2913bbd2-ae8a-5f71-8c99-4fb6c76f3a91"
version = "0.33.16"

[[deps.StatsFuns]]
deps = ["ChainRulesCore", "HypergeometricFunctions", "InverseFunctions", "IrrationalConstants", "LogExpFunctions", "Reexport", "Rmath", "SpecialFunctions"]
git-tree-sha1 = "25405d7016a47cf2bd6cd91e66f4de437fd54a07"
uuid = "4c63d2b9-4356-54db-8cca-17b64c39e42c"
version = "0.9.16"

[[deps.SuiteSparse]]
deps = ["Libdl", "LinearAlgebra", "Serialization", "SparseArrays"]
uuid = "4607b0f0-06f3-5cda-b6b1-a6196a1729e9"

[[deps.TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"

[[deps.TableTraits]]
deps = ["IteratorInterfaceExtensions"]
git-tree-sha1 = "c06b2f539df1c6efa794486abfb6ed2022561a39"
uuid = "3783bdb8-4a98-5b6b-af9a-565f29a5fe9c"
version = "1.0.1"

[[deps.Tables]]
deps = ["DataAPI", "DataValueInterfaces", "IteratorInterfaceExtensions", "LinearAlgebra", "OrderedCollections", "TableTraits", "Test"]
git-tree-sha1 = "5ce79ce186cc678bbb5c5681ca3379d1ddae11a1"
uuid = "bd369af6-aec1-5ad0-b16a-f7cc5008161c"
version = "1.7.0"

[[deps.Tar]]
deps = ["ArgTools", "SHA"]
uuid = "a4e569a6-e804-4fa4-b0f3-eef7a1d5b13e"

[[deps.Test]]
deps = ["InteractiveUtils", "Logging", "Random", "Serialization"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"

[[deps.nghttp2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850ede-7688-5339-a07c-302acd2aaf8d"

[[deps.p7zip_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "3f19e933-33d8-53b3-aaab-bd5110c3b7a0"
"""

# ╔═╡ Cell order:
# ╟─b6ca4a64-a546-11ec-1bad-139b7b294d3d
# ╟─d5215c6d-15b3-44b2-84c7-a54148fef7fd
# ╟─a1b2a449-271c-4437-a4cc-08243b7506bb
# ╟─9959fdd6-9135-4154-9085-3d053c720369
# ╟─d4ea373d-d4e3-4a42-9eec-4b158a1f5d07
# ╠═94b1d0e9-e33a-411f-b2e2-e52ac22b5815
# ╟─2c353035-7994-4b4f-bd67-23ec2077f598
# ╠═eea6c43b-61fe-4b4a-a67a-d2dc98a7ec61
# ╠═a2803c70-8de4-4952-9577-68bead54f6ae
# ╟─510f7e94-0089-4ea1-ad2b-3e4fa58c0a1f
# ╟─d778079f-2d2a-4d63-99ee-233c691f84ef
# ╟─0c52ea76-e202-4f32-b5ae-a6bc0e268bab
# ╟─e14a833e-eca7-42fa-8091-1279b4fc03b8
# ╠═b626a9c3-a1f3-440f-8af2-fc23b703d993
# ╟─15bafb5c-0962-4d0f-a0c2-9721a797883d
# ╟─da1aced9-9c44-4bb0-a9c9-252086ea401b
# ╠═b423b186-ebc6-4665-a434-f4f5145d94e4
# ╟─c2dc2717-7b16-4eb7-9df1-5b88c4f5e73f
# ╠═f9646774-72e8-4db8-b271-35e6bbf41977
# ╟─f49ec026-62ff-4eb9-89a9-ca4a3a0d44f9
# ╠═21968242-efda-4902-8e93-d8af6f31ec25
# ╟─62f6c294-3684-4eeb-bfbd-06c10dc7fdc6
# ╠═73682d5d-e2de-4fcf-b416-80744a6f5481
# ╟─5934c827-508b-4249-9f4a-20d74a5a8de0
# ╟─4cbfb6fb-9198-4522-b5b0-af3567458a79
# ╠═31bab126-68cf-4cc8-a848-8477a70d6044
# ╟─d35d3327-16c2-44c1-b09d-15d64e9a9430
# ╟─7ef684b1-9ae5-4cc6-b507-053c8e38492f
# ╟─ad9cc2f7-142e-49ee-9921-b44f86d234db
# ╠═ce7d95b3-84df-4980-aaee-0e16acb72400
# ╠═dd3c6f40-cf8b-43f5-99c2-2dd2877e4844
# ╟─15559aed-2307-4eee-9269-77599b720382
# ╟─419b5bc2-a83b-443e-a56d-d686e63c5c54
# ╠═ca4fc529-66dc-41dc-aada-f882b310ee9c
# ╟─58ddc742-7b49-4ab6-81ac-74065380af9d
# ╟─f8a63f8b-dc33-40fe-98ea-23445c2d9790
# ╟─1a3bc865-2bb4-419c-b007-1eb3e9827b20
# ╠═cf363f68-75bf-43e2-9e76-ae374256fca1
# ╟─75df6409-5d9e-4c10-bf75-8c102d61edec
# ╠═fedaec59-23ec-4e44-a468-3b85ecf06835
# ╟─499b6f25-3ed9-4135-931c-d4f15e954aec
# ╠═7dfeb9a5-0276-4218-81b6-4a59fa4f7ebd
# ╟─8202d4c3-f18c-4d48-b689-080701b76589
# ╟─19abed41-4860-452e-b1ea-a2e30d29b49f
# ╠═6ee8853f-cbff-4207-af59-053e9cd7daf5
# ╟─d63b0870-c1f1-47c0-9946-d97fc11127c8
# ╟─476d4097-77ee-4bfb-80a0-6d5c0f5ffa2c
# ╟─3231be0f-3a66-4214-8d2f-c362308f3650
# ╠═bdb6da88-d156-4197-9a45-e6c5b0543d10
# ╟─bdd096fd-5822-41b8-ae6f-9c25c127fae3
# ╟─98d3b087-3402-43c1-bcbb-766b70ca5dde
# ╟─820a4a9f-d6da-4cb9-b4cb-92e834ba0069
# ╟─de562746-0a7e-4f22-b30a-6f145d429583
# ╠═9d48819f-6278-483f-b34a-6a5410bae48c
# ╟─0fccc3cb-5f8a-4d0d-9e8c-10d97361154c
# ╠═60995ad2-5da2-4582-b1de-498eb3d31961
# ╟─24ef595f-e4f3-4ab6-aa16-0ccb995ccf68
# ╠═f9416c50-aef9-448d-9b24-53235997bbcf
# ╟─879c636d-cb6b-4312-84ac-ddc6cb228514
# ╠═e80898b3-e0c5-4363-836a-d157719dfd0a
# ╟─40bbd304-b372-4bcc-ac02-b32fd3a6f76a
# ╟─d2453234-f8e3-4f46-9350-92a0287e4df3
# ╠═5c6e701c-af10-4f51-aa63-0e291d9dbdd4
# ╟─9c9224f3-01cd-49f7-98bf-252b46ea02ed
# ╟─25416763-56e5-4db2-891a-d3c4bd352a30
# ╟─878f253a-e36e-4c33-9f73-305bbfcb47e8
# ╟─6580bd8d-5486-45f7-88cf-81d180253404
# ╟─038fb6f0-ac14-4a66-ab03-6097ed3f79e4
# ╟─bc9962c4-1a02-42f2-83da-ebaf9800ebcb
# ╟─533a6ce8-4e91-41d1-986c-91df884e29cb
# ╟─c827f930-9042-47f4-89ff-849f69550084
# ╟─a44013a6-fce8-49e7-9278-884a4635f13a
# ╟─097bfc70-b44c-4e6f-a456-a80d12c5c3a2
# ╟─14ca1f9f-243a-46b2-9c06-1ebf550ed930
# ╟─219001df-0c33-4d16-9ab0-36545f59ffa1
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
