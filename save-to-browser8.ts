import * as idb from 'idb'

// Store form values in an IndexedDB for later retrieval by `name`
async function saveFormToBrowser(formID = 'form_assignment', dbName = 'assignment', storeName = 'assignment') {
  // Open the saved assignment data as an IDB database
  let assignmentDB = await idb.openDB(dbName)

  // Get the form element
  const formElement = document.getElementById(formID) as HTMLFormElement

  // Create an array of the form's fields
  const formFieldsArray = Array.from(formElement.elements)

  // Store all form field values in DB
  formFieldsArray.map( field => assignmentDB.put(storeName, field.value, field.id) )
}

export { saveFormToBrowser }